<!DOCTYPE HTML>
<html dir="ltr" lang="th">
<!-- Top Head -->
<?php include("incs/head-top.html") ?>
<!-- /Top Head -->

<body class="page-profile">
<script>
  //<![CDATA[
  $(document).ready(function(){
	  //$('#navigation>ul>li:nth-child(1)>a').addClass('selected');
  });
  //]]>
</script>
<!-- Headbar -->
<?php include("incs/header.html") ?>
<!-- /Headbar -->

   <div id="toc">
		<div class="container">
		<section class="sec-favorite pt0">

				<h2 class="h-topic"><span>ชั้นหนังสือ</span></h2>
                <ul class="my-tabs idTabs mb20-xs mb30-md">
                    <li><a class="selected" href="#shelf1" title="อีบุ๊ค">อีบุ๊ค</a></li>
                    <li><a href="#shelf2" title="นวนิยาย บนเว็บไซต์">นวนิยาย บนเว็บไซต์</a></li>
                </ul>
                <div class="contentTabs">
                    <!-- Fav1 -->    
                    <div class="bx-tab" id="shelf1">
                    <nav class="bar-paging d-flex center-xs end-xsh mb20-xs">
                        <ul class="pagination">
                            <li class="page-item">
                            <a class="page-link" href="#" aria-label="Previous">
                                <span aria-hidden="true"><img src="./assets/imgs/ic-prev-page.png" height="12"></span>
                            </a>
                            </li>
                            <li class="page-item active"><a class="page-link" href="#">1</a></li>
                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item"><a class="page-link" href="#">4</a></li>
                            <li class="page-item"><a class="page-link" href="#">5</a></li>
                            <li class="page-item">
                            <a class="page-link" href="#" aria-label="Next">
                                <span aria-hidden="true"><img src="./assets/imgs/ic-next-page.png" height="12"></span>
                            </a>
                            </li>
                        </ul>
                    </nav>
                    <div class="cb thm-book row _chd-cl-xs-06-xsh-04-sm-03-md-25 start-xs">
                        <?php for($i=1; $i<=15; $i++) { 

                            $img_url = "assets/contents/thm-book-05.png";
                            $name = "EXE #2 Light Novel";
                            $price = "฿70";
                            $author = "ผู้การ F";
                            switch($i%5)
                            {
                                case "1": 
                                            $img_url = "assets/contents/thm-book-01.png";
                                            $name = "Black Cover บันทึกของกระทิง";
                                            $price = "฿75";
                                            $author = "Jonny Onda";
                                    break;
                                case "2": 
                                            $img_url = "assets/contents/thm-book-02.png";
                                            $name = "Tokyo Ghoul re (เควส)";
                                            $price = "฿70";
                                            $author = "Shin Towada";
                                    break;
                                case "3": 
                                            $img_url = "assets/contents/thm-book-03.png";
                                            $name = "ONE PIECE FILM GOLD";
                                            $price = "฿66.50";
                                            $author = "Eiichiro Oda";
                                    break;
                                case "4": 
                                            $img_url = "assets/contents/thm-book-04.png";
                                            $name = "Executional ตำนานวีรบุรุษโลก(ไม่อยาก)จำ";
                                            $price = "฿149";
                                            $author = "Nightberry";
                                    break;
                            }
                            ?>
                            <article id="ar<?php echo $i; ?>">
                                <div class="in">
                                    <figure><a href="#" title="<?php echo $name; ?>"><img src="<?php echo $img_url; ?>" alt="<?php echo $name; ?>"></a>
                                    <i class="tag-new">new</i></figure>
                                    <div class="detail">
                                        <h3><a href="#" title="<?php echo $name; ?>"><?php echo $name; ?></a></h3>
                                        <p class="author"><?php echo $author; ?></p>
                                        <!-- <div class="bar-price">
                                            <span class="price"><b>299</b> บาท</span>
                                            <span class="old">999 บาท</span>
                                        </div> -->
                                    </div>
                                </div>
                            </article>
                            
                        <?php } ?>
                    </div>    

                    <nav class="bar-paging d-flex center-xs end-xsh mt20-xs">
                        <ul class="pagination">
                            <li class="page-item">
                            <a class="page-link" href="#" aria-label="Previous">
                                <span aria-hidden="true"><img src="./assets/imgs/ic-prev-page.png" height="12"></span>
                            </a>
                            </li>
                            <li class="page-item active"><a class="page-link" href="#">1</a></li>
                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item"><a class="page-link" href="#">4</a></li>
                            <li class="page-item"><a class="page-link" href="#">5</a></li>
                            <li class="page-item">
                            <a class="page-link" href="#" aria-label="Next">
                                <span aria-hidden="true"><img src="./assets/imgs/ic-next-page.png" height="12"></span>
                            </a>
                            </li>
                        </ul>
                    </nav>


                    </div>
                    <!-- Fav2 -->
                    <div class="bx-tab" id="shelf2">
                     2
                    </div>
                </div>

		</section>



		</div>
  </div>

<!-- footer -->
<?php include("incs/footer.html") ?>
<?php include("incs/lightbox.html") ?>
<!-- /footer -->
<!-- js -->
<?php include("incs/js.html") ?>
<script src="assets/js/jquery.idTabs.min.js"></script>
<!-- /js -->

</body>
</html>