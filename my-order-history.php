<!DOCTYPE HTML>
<html dir="ltr" lang="th">
<!-- Top Head -->
<?php include("incs/head-top.html") ?>
<!-- /Top Head -->

<body class="page-profile">
<script>
  //<![CDATA[
  $(document).ready(function(){
	  //$('#navigation>ul>li:nth-child(1)>a').addClass('selected');
  });
  //]]>
</script>
<!-- Headbar -->
<?php include("incs/header.html") ?>
<!-- /Headbar -->

   
   
   <div id="toc">
		<div class="container">
		<section class="sec-01 pt0">

				<h2 class="h-topic"><span>จัดการบัญชี</span></h2>
                <ul class="my-tabs mb20-xs mb30-md">
                    <li><a href="profile.php" title="ข้อมูลส่วนตัว">ข้อมูลส่วนตัว</a></li>
                    <li><a class="selected" href="my-order-history.php" title="รายการคำสั่งซื้อล่าสุด">รายการคำสั่งซื้อล่าสุด</a></li>
                </ul>
                
                <div class="tb-order-history table-responsive table-responsive-sm table-drag" onscorll="$(this).addClass('off');">
				    <table class="table">
                        <thead>
                            <tr>
                            <th class="txt-l">คำสังซื่อ #</th>
                            <th align="center" class="txt-c">สั่งซื้อวันที่</th>
                            <th align="center" class="txt-c">จำนวนสินค้า</th>
                            <th align="center" class="txt-c">ยอดรอมทั้งสิ้น</th>
                            <th align="center" class="txt-l">รายละเอียดสินค้า</th>
                            </tr>
                        </thead>
						<tbody>
                            <!-- group -->
						    <tr>
							    <td style="width:20%"> 211265585080805</td>
							    <td align="center" style="width:20%">12/09/2018</td>
							    <td align="center" style="width:10%">5</td>
								<td align="center" style="width:10%"> 1,000.00</td>
								<td align="left" style="width:40%">
                                    <a class="sw-tb" href="javascript:;" title="view more detail" onClick="$(this).toggleClass('active').parents('tr').next('.sub-table').find('.sub-tmp').slideToggle();">ดูรายการสินค้า <i class="yicon"><img src="./assets/imgs/ic-angle-down-red.png" height="10"></i></a>

								</td>
							</tr>
							<tr class="sub-table">
                                <td colspan="5">
                                    <div class="sub-tmp" style="display: none">
                                        <table class="table">
                                            <?php for($i=1;$i<=3;$i++){?> 
                                            
                                            <tr>
                                                <td class="pa0-xs" colspan="2" valign="top" style="width:40%">
                                                <div class="thm-left">
                                                    <article>
                                                        <div class="in">
                                                            <figure><a href="#" title="Black Cover บันทึกของกระทิง"><img src="assets/contents/thm-book-01.png" alt="Black Cover บันทึกของกระทิง"></a></figure>
                                                            <div class="detail">
                                                                <h3><a href="#" title="Black Cover บันทึกของกระทิง">Black Cover บันทึกของกระทิง</a></h3>
                                                                <div class="type"><i class="yicon"><img src="./assets/imgs/ic-noun-book.png"></i> หนังสือ</div>
                                                            </div>
                                                        </div>
                                                    </article>
                                                </div>
                                                </td>
                                                <td class="pa0-xs" align="center" valign="top" style="width:10%">1</td>
                                                <td class="pa0-xs" align="center" valign="top" style="width:10%"> 259.00</td>
                                                <?php if($i==1){?> 
                                                <td class="pa0-xs" rowspan="3" align="left" valign="top" style="width:40%">
                                                <b>ที่อยู่ในการจัดส่ง</b>
                                                <address>19 ซอย ไผ่สิงห์โต ถ.พระราม4 คลองเตย, 10110, คลองเตย/ Khlong Toei, กรุงเทพมหานคร/ Bangkok</address>
                                                <div class="mt20-xs">
                                                    <b>ชำระเงินโดย</b>
                                                    <p>บัตรเครดิต/บัตรเดบิต</p>
                                                </div>
                                                
                                                </td>
                                                <?php } ?>
                                            </tr>
                                            <?php } ?>
                                        </table>
                                    </div>

                                </td>
							</tr>
                            <!-- /group -->
                            <!-- group -->
						    <tr>
							    <td style="width:20%"> 211265585080805</td>
							    <td align="center" style="width:20%">12/09/2018</td>
							    <td align="center" style="width:10%">5</td>
								<td align="center" style="width:10%"> 1,000.00</td>
								<td align="left" style="width:40%">
                                    <a class="sw-tb active" href="javascript:;" title="view more detail" onClick="$(this).toggleClass('active').parents('tr').next('.sub-table').find('.sub-tmp').slideToggle();">ดูรายการสินค้า <i class="yicon"><img src="./assets/imgs/ic-angle-down-red.png" height="10"></i></a>

								</td>
							</tr>
							<tr class="sub-table">
                                <td colspan="5">
                                    <div class="sub-tmp" style="display: block">
                                        <table class="table">
                                            <?php for($i=1;$i<=3;$i++){?> 
                                            
                                            <tr>
                                                <td class="pa0-xs" colspan="2" valign="top" style="width:40%">
                                                <div class="thm-left">
                                                    <article>
                                                        <div class="in">
                                                            <figure><a href="#" title="Black Cover บันทึกของกระทิง"><img src="assets/contents/thm-book-01.png" alt="Black Cover บันทึกของกระทิง"></a></figure>
                                                            <div class="detail">
                                                                <h3><a href="#" title="Black Cover บันทึกของกระทิง">Black Cover บันทึกของกระทิง</a></h3>
                                                                <div class="type"><i class="yicon"><img src="./assets/imgs/ic-noun-book.png"</i> หนังสือ</div>
                                                            </div>
                                                        </div>
                                                    </article>
                                                </div>
                                                </td>
                                                <td class="pa0-xs" align="center" valign="top" style="width:10%">1</td>
                                                <td class="pa0-xs" align="center" valign="top" style="width:10%"> 259.00</td>
                                                <?php if($i==1){?> 
                                                <td class="pa0-xs" rowspan="3" align="left" valign="top" style="width:40%">
                                                    <b>ที่อยู่ในการจัดส่ง</b>
                                                    <address>19 ซอย ไผ่สิงห์โต ถ.พระราม4 คลองเตย, 10110, คลองเตย/ Khlong Toei, กรุงเทพมหานคร/ Bangkok</address>
                                                    <div class="mt20-xs">
                                                        <b>ชำระเงินโดย</b>
                                                        <p>บัตรเครดิต/บัตรเดบิต</p>
                                                    </div>
                                                
                                                </td>
                                                <?php } ?>
                                            </tr>
                                            <?php } ?>
                                        </table>
                                    </div>

                                </td>
							</tr>
                            <!-- /group -->
                            <!-- group -->
						    <tr>
							    <td style="width:20%"> 211265585080805</td>
							    <td align="center" style="width:20%">12/09/2018</td>
							    <td align="center" style="width:10%">5</td>
								<td align="center" style="width:10%"> 1,000.00</td>
								<td align="left" style="width:40%">
                                    <a class="sw-tb" href="javascript:;" title="view more detail" onClick="$(this).toggleClass('active').parents('tr').next('.sub-table').find('.sub-tmp').slideToggle();">ดูรายการสินค้า <i class="yicon"><img src="./assets/imgs/ic-angle-down-red.png" height="10"></i></a>

								</td>
							</tr>
							<tr class="sub-table">
                                <td colspan="5">
                                    <div class="sub-tmp" style="display: none">
                                        <table class="table">
                                            <?php for($i=1;$i<=3;$i++){?> 
                                            
                                            <tr>
                                                <td class="pa0-xs" colspan="2" valign="top" style="width:40%">
                                                <div class="thm-left">
                                                    <article>
                                                        <div class="in">
                                                            <figure><a href="#" title="Black Cover บันทึกของกระทิง"><img src="assets/contents/thm-book-01.png" alt="Black Cover บันทึกของกระทิง"></a></figure>
                                                            <div class="detail">
                                                                <h3><a href="#" title="Black Cover บันทึกของกระทิง">Black Cover บันทึกของกระทิง</a></h3>
                                                                <div class="type"><i class="yicon"><img src="./assets/imgs/ic-noun-book.png"</i> หนังสือ</div>
                                                            </div>
                                                        </div>
                                                    </article>
                                                </div>
                                                </td>
                                                <td class="pa0-xs" align="center" valign="top" style="width:10%">1</td>
                                                <td class="pa0-xs" align="center" valign="top" style="width:10%"> 259.00</td>
                                                <?php if($i==1){?> 
                                                <td class="pa0-xs" rowspan="3" align="left" valign="top" style="width:40%">
                                                <b>ที่อยู่ในการจัดส่ง</b>
                                                <address>19 ซอย ไผ่สิงห์โต ถ.พระราม4 คลองเตย, 10110, คลองเตย/ Khlong Toei, กรุงเทพมหานคร/ Bangkok</address>
                                                <div class="mt20-xs">
                                                    <b>ชำระเงินโดย</b>
                                                    <p>บัตรเครดิต/บัตรเดบิต</p>
                                                </div>
                                                
                                                </td>
                                                <?php } ?>
                                            </tr>
                                            <?php } ?>
                                        </table>
                                    </div>

                                </td>
							</tr>
                            <!-- /group -->
                            
                        </tbody>    
                    </table>
                </div>			
											  

		</section>



		</div>
  </div>

<!-- footer -->
<?php include("incs/footer.html") ?>
<?php include("incs/lightbox.html") ?>
<!-- /footer -->
<!-- js -->
<?php include("incs/js.html") ?>
<!-- /js -->

</body>
</html>