<!DOCTYPE HTML>
<html dir="ltr" lang="th">
<!-- Top Head -->
<?php include("incs/head-top.html") ?>
<!-- /Top Head -->

<body class="page-book">
<script>
  //<![CDATA[
  $(document).ready(function(){
	  $('#navigation>ul>li:nth-child(7)>a').addClass('selected');
  });
  //]]>
</script>
<!-- Headbar -->
<?php include("incs/header.html") ?>
<!-- /Headbar -->

   
<div id="toc" class="pt10">
	
	<div class="container">
        <div class="crumb"><a href="#">หน้าแรก</a> / <a href="#">นิยาย</a> / <a href="#">Box Set Limited Edition (นิยาย)</a> / <span>กล่องเปล่ามหัศจรรย์รักนายนกกระจอกเทศ</span></div>
		<section class="sec-main-book">
        <!-- content -->
        <article class="content">
				
            <div class="head-book">

                <div class="thm">
                    <!-- <figure>
                        <img src="./assets/contents/thm-book-01.png" alt="กล่องเปล่ามหัศจรรย์รักนายนกกระจอกเทศ">
                    </figure> -->
					<div class="book-slider">
						<div class="slider slider-for">
							<div>
							<a data-fancybox="gr" data-caption="กล่องเปล่ามหัศจรรย์รักนายนกกระจอกเทศ" href="./assets/contents/thm-book-01.png"><img src="./assets/contents/thm-book-01.png" alt="กล่องเปล่ามหัศจรรย์รักนายนกกระจอกเทศ"></a>
							</div>
							<div>
							<a data-fancybox="gr" data-caption="กล่องเปล่ามหัศจรรย์รักนายนกกระจอกเทศ" href="./assets/contents/thm-book-02.png"><img src="./assets/contents/thm-book-02.png" alt="กล่องเปล่ามหัศจรรย์รักนายนกกระจอกเทศ"></a>
							</div>
							<div>
							<a data-fancybox="gr" data-caption="กล่องเปล่ามหัศจรรย์รักนายนกกระจอกเทศ" href="./assets/contents/thm-book-03.png"><img src="./assets/contents/thm-book-03.png" alt="กล่องเปล่ามหัศจรรย์รักนายนกกระจอกเทศ"></a>
							</div>
							<div>
							<a data-fancybox="gr" data-caption="กล่องเปล่ามหัศจรรย์รักนายนกกระจอกเทศ" href="./assets/contents/thm-book-04.png"><img src="./assets/contents/thm-book-04.png" alt="กล่องเปล่ามหัศจรรย์รักนายนกกระจอกเทศ"></a>
							</div>
							<div>
							<a data-fancybox="gr" data-caption="กล่องเปล่ามหัศจรรย์รักนายนกกระจอกเทศ" href="./assets/contents/thm-book-05.png"><img src="./assets/contents/thm-book-05.png" alt="กล่องเปล่ามหัศจรรย์รักนายนกกระจอกเทศ"></a>
							</div>
						</div>
						<div class="slider slider-nav">
							<div>
							<img src="./assets/contents/thm-book-01.png" alt="กล่องเปล่ามหัศจรรย์รักนายนกกระจอกเทศ">
							</div>
							<div>
							<img src="./assets/contents/thm-book-02.png" alt="กล่องเปล่ามหัศจรรย์รักนายนกกระจอกเทศ">
							</div>
							<div>
							<img src="./assets/contents/thm-book-03.png" alt="กล่องเปล่ามหัศจรรย์รักนายนกกระจอกเทศ">
							</div>
							<div>
							<img src="./assets/contents/thm-book-04.png" alt="กล่องเปล่ามหัศจรรย์รักนายนกกระจอกเทศ">
							</div>
							<div>
							<img src="./assets/contents/thm-book-05.png" alt="กล่องเปล่ามหัศจรรย์รักนายนกกระจอกเทศ">
							</div>
						</div>
					</div>
                    <!-- <ul class="book-author mt30-xs">
                        <li class="_chd-cl-xs"><em class="_self-cl-xs-05-md-04 txt-r">หมวดหมู่ :</em> <span>-</span></li>
                    </ul> -->
                </div>
                <div class="detail">
                    <h1>กล่องเปล่ามหัศจรรย์รักนายนกกระจอกเทศ</h1>
                    <div class="ctrl-btn _flex start-xs _chd-mr30">
						
                        <a class="ui-btn-border-gray" href="javascript:;" onClick="$(this).toggleClass('active')" title="เพิ่มรายการที่ชื่นชอบ"><i class="yicon fav"><img src="./assets/imgs/my-favorite.png" height="16"></i> เพิ่มรายการที่ชื่นชอบ</a>
                        <a class="ui-btn-border-gray" href="javascript:;" data-fancybox="share" data-src="#popup-share" title="แชร์เรื่องนี้"><i class="yicon"><img src="./assets/imgs/ic-share.png" height="16"></i> แชร์เรื่องนี้</a>
                        
                        <!--<a class="ui-btn-blue-sq" href="full-read-ep.php" title="ทดลองอ่าน"><i class="ic-view-wh"></i> ทดลองอ่าน</a>-->
                    </div>

                    <div class="type-cart d-flex center-xs start-xsh top-xs">
                        <div class="t1 d-flex flex-column center-xs mr20-xs">
                            <div class="tags f-bold"> <i class="yicon d-inline mr5-xs"><img src="./assets/imgs/ic-products.png" height="24"></i>สินค้า</div>
                            <div class="price f-big _self-mv10"><b class="t-red">159</b> บาท</div>
                            <div class="add-cart">
								<a href="javascript:;" class="ui-btn-cart btn-md btn-block mb20-xs"><i class="yicon"><img src="./assets/imgs/ic-cart-wh.png" height="20"></i> <small>เพิ่มลงตระกร้าสินค้า</small></a>
							</div>
                        </div>
                    </div>

                    <div class="abstract-body mt30-xs">
						<h3 class="h-text f-normal">กล่องใส่หนังสือ </h3>
                        <p class="desc">กล่องพลาสติกใส่หนังสือ สำหรับสาวกนวนิยาย และทุกเพศ ทุกวัย ผลิตจากพลาสติกอย่างดี สวยงาม แข็งแรง ทนทานค่ะ  ช่วยดูแลหนังสือของคุณ ประหยัดเนื้อที่การจัดเก็บ ดูแลง่าย</p>
                    </div>
                    

                </div>

            </div>
            
            <div class="reader body-text">

                <!-- <div class="abstract-body">
                    <b class="hid">เรื่องย่อ</b>
                    <p>นับแต่นั้น เขาก็ได้แปรเปลี่ยนจากมัจฉาเป็นมังกร  เขารุ่งโรจน์ราวดารา เหยียบย่างในเส้นทางของผู้ฝึกตนอันเป็นตำนาน จากมดปลวกเล็กจ้อย ณ จุดต่ำสุดของโลกเติบโตขึ้นทีละก้าวไปยังสำนักมากอำนาจ สำนักโบราณอันแข็งแกร่ง เผชิญอัจฉริยะจำนวนนับไม่ถ้วนนี่คือยุคแห่งตำนาน!</p>
                </div> -->
                
                

            </div>
        </article>
        <!-- /content -->

        <div class="bx related space-top">
		    <div class="head-title center-xs start-md">
			<h2 class="h-topic"><a class="t-black" href="cltegory-list.php" title="เรื่องที่เนื้อหาคล้ายกัน">เรื่องที่เนื้อหาคล้ายกัน</a></h2>
			</div>

			
			<div class="slider promotion-book thm-book">
			<?php for($i=1; $i<=15; $i++) { 

				$img_url = "assets/contents/thm-book-05.png";
				$name = "EXE #2 Light Novel";
				$price = "฿70";
				$author = "ผู้การ F";
				switch($i%5)
				{
					case "1": 
								$img_url = "assets/contents/thm-book-01.png";
								$name = "Black Cover บันทึกของกระทิง";
								$price = "฿75";
								$author = "Jonny Onda";
						break;
					case "2": 
								$img_url = "assets/contents/thm-book-02.png";
								$name = "Tokyo Ghoul re (เควส)";
								$price = "฿70";
								$author = "Shin Towada";
						break;
					case "3": 
								$img_url = "assets/contents/thm-book-03.png";
								$name = "ONE PIECE FILM GOLD";
								$price = "฿66.50";
								$author = "Eiichiro Oda";
						break;
					case "4": 
								$img_url = "assets/contents/thm-book-04.png";
								$name = "Executional ตำนานวีรบุรุษโลก(ไม่อยาก)จำ";
								$price = "฿149";
								$author = "Nightberry";
						break;
				}
				?>
				<div>
				<article id="ar<?php echo $i; ?>">
					<div class="in">
						<figure><a href="book-detail.php#" title="<?php echo $name; ?>"><img src="<?php echo $img_url; ?>" alt="<?php echo $name; ?>"></a>
						<!--i class="tag-new">new</i--></figure>
						<div class="detail">
							<h3><a href="#" title="<?php echo $name; ?>"><?php echo $name; ?></a></h3>
							<!-- <p class="author"><?php echo $author; ?></p> -->
							<div class="bar-price">
								<span class="price"><b><?php echo $price; ?></b> บาท</span>
								<span class="old">999 บาท</span>
							</div>
						</div>
					</div>
				</article>
				</div>
			<?php } ?>
			</div>
        </div>

		</section>
		
		
	</div>	
  </div>

<!-- footer -->
<?php include("incs/footer.html") ?>
<?php include("incs/lightbox.html") ?>
<!-- /footer -->
<!-- js -->
<?php include("incs/js.html") ?>
<script type="text/javascript" src="assets/js/slick.min.js"></script>
<script>
$('.slider-for').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: false,
  fade: true,
  asNavFor: '.slider-nav'
});
$('.slider-nav').slick({
  slidesToShow: 3,
  slidesToScroll: 1,
  asNavFor: '.slider-for',
  dots: false,
  centerMode: false,
  focusOnSelect: true
});
//related	
$('.promotion-book').slick({
  dots: false,
  infinite: true,
  arrows: true,
  speed: 300,
  autoplaySpeed: 5000,
  slidesToShow: 5,
  slidesToScroll: 5,
  //variableWidth: true,
  responsive: [
    {
      breakpoint: 800,
      settings: {
	  	//centerPadding: '60px',
        slidesToShow: 3,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 480,
      settings: {
	  	//centerPadding: '40px',
        slidesToShow: 2,
        slidesToScroll: 1
      }
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ]
});

</script>
<!-- /js -->

</body>
</html>
