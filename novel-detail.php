<!DOCTYPE HTML>
<html dir="ltr" lang="th">
<!-- Top Head -->
<?php include("incs/head-top.html") ?>
<!-- /Top Head -->

<body class="page-book">
<script>
  //<![CDATA[
  $(document).ready(function(){
	  $('#navigation>ul>li:nth-child(2)>a').addClass('selected');
  });
  //]]>
</script>
<!-- Headbar -->
<?php include("incs/header.html") ?>
<!-- /Headbar -->

   
<div id="toc" class="pt10">
	
	<div class="container">
        <div class="crumb"><a href="#">หน้าแรก</a> / <a href="#">นิยาย</a> / <a href="#">นิยายรักโรมานติก</a> / <span>สิรินนารี</span></div>
		<section class="sec-main-book">
        <!-- content -->
        <article class="content">
				
            <div class="head-book">

                <div class="thm">
                    <figure>
                        <img src="./assets/contents/thm-book-02.png" alt="สิรินนารี">
                    </figure>
                    <ul class="book-author mt30-xs">
                        <li class="_chd-cl-xs"><em class="_self-cl-xs-05-md-04 txt-r">ผู้เขียน :</em>  <a href="#" class="t-blue">จุฬามณี</a></li>
                        <li class="_chd-cl-xs"><em class="_self-cl-xs-05-md-04 txt-r">หมวดหมู่ :</em> <a href="#" class="t-blue">นิยายรักโรมานติก</a>
                    </ul>
                </div>
                <div class="detail">
                    <h1>สิรินนารี</h1>
                    <div class="ctrl-btn _flex start-xs _chd-mr30">
                        <a class="ui-btn-border-gray" href="javascript:;" onClick="$(this).toggleClass('active')" title="เพิ่มรายการที่ชื่นชอบ"><i class="yicon fav"><img src="./assets/imgs/my-favorite.png" height="16"></i> เพิ่มรายการที่ชื่นชอบ</a>
                        <a class="ui-btn-border-gray" href="javascript:;" data-fancybox="share" data-src="#popup-share" title="แชร์เรื่องนี้"><i class="yicon"><img src="./assets/imgs/ic-share.png" height="16"></i> แชร์เรื่องนี้</a>
                        
                        <!--<a class="ui-btn-blue-sq" href="read-novel.php" title="ทดลองอ่าน"><i class="ic-view-wh"></i> ทดลองอ่าน</a>-->
                    </div>

                    <div class="type-cart d-flex center-xs start-xsh">
                        <div class="t1 d-flex flex-column center-xs">
                            <div class="tags"> <i class="yicon d-inline mr5-xs"><img src="./assets/imgs/ic-novel.png" height="24"></i>นวนิยาย บนเว็บไซต์</div>
                            <div class="price f-big _self-mv10"><b class="t-red">259</b> บาท</div>
                            <div class="add-cart"><a href="javascript:;" class="ui-btn-red btn-md btn-block"><i class="yicon"><img src="./assets/imgs/ic-coin.png" height="16"></i> ซื้อทุกตอน</a></div>
                        </div>

                    </div>

                    <div class="abstract-body mt30-xs">
                        <p class="desc">ผู้หญิงสองคนต่างมีวิถีชีวิต นิสัยใจคอ สถานะทางสังคม ความฝัน และมีคู่รักของตนเอง กลับกลายต้องมาสลับร่างกัน... แล้วจะยอมอยู่ในร่างคนที่ไม่ใช่ตนได้อย่างไร แต่เอ๊ะ หรือนี่อาจจะเป็นการสลับร่างเพื่อตามหาคู่แท้กันแน่</p>
                    </div>
                    

                </div>

            </div>
            
            <div class="reader body-text">

                <!-- <div class="abstract-body">
                    <b class="hid">เรื่องย่อ</b>
                    <p>นับแต่นั้น เขาก็ได้แปรเปลี่ยนจากมัจฉาเป็นมังกร  เขารุ่งโรจน์ราวดารา เหยียบย่างในเส้นทางของผู้ฝึกตนอันเป็นตำนาน จากมดปลวกเล็กจ้อย ณ จุดต่ำสุดของโลกเติบโตขึ้นทีละก้าวไปยังสำนักมากอำนาจ สำนักโบราณอันแข็งแกร่ง เผชิญอัจฉริยะจำนวนนับไม่ถ้วนนี่คือยุคแห่งตำนาน!</p>
                </div> -->
                
                
                <!-- EP -->
                <div class="zone-episode">
                    <div class="head-title mb15 center-xs start-md">
                        <h2 class="h-text"><span>ตอน ทั้งหมด</span></h2>
                        <small>20 ตอน (ยังไม่จบ)</small>
                    </div>
                    
                    <div class="table-drag">
                        <table class="table-sq tb-hover" width="100%" border="0">
                                        <thead>
                                        <tr>
                                        <th scope="col" valign="middle" align="left">ตอนที่ <i class="fa-sort"></i></th>
                                        <th scope="col" valign="middle" align="center">&nbsp;</th>
                                        <th scope="col" valign="middle" align="left">ราคาขาย</th>
                                        <th scope="col" valign="middle" align="center">วันที่อัพเดต</th>
                                        <th scope="col" valign="middle" align="center">ผู้ชมทั้งหมด</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                        <td valign="middle"><a href="read-novel.php">ตอนที่ 24: ลำแสงที่พุ่งมา</a><br><em class="t-gray2">(รอทีมงานตรวจ)</em></td>
                                        <td valign="middle">&nbsp;</td>
                                        <td valign="middle"><a class="t-gray" href="javascript:;" data-fancybox="" data-src="#popup-topup-buy"><i class="yicon"><img src="./assets/imgs/ic-large-coin.png" height="20"></i> 20 เหรียญ</a></td>
                                        <td valign="middle" align="center">อัพเดต 20 พ.ย. 2562</td>
                                        <td valign="middle" align="center"><i class="yicon"><img src="./assets/imgs/ic-view.png" height="12"></i> 1,000</td>
                                        </tr>

                                        <tr>
                                        <td valign="middle"><a href="read-novel.php">ตอนที่ 23: ลำแสงที่พุ่งมา</a></td>
                                        <td valign="middle">&nbsp;</td>
                                        <td valign="middle"><a class="t-gray" href="javascript:;" data-fancybox="" data-src="#popup-confirm-buy"><i class="yicon"><img src="./assets/imgs/ic-large-coin.png" height="20"></i> 20 เหรียญ</a></td>
                                        <td valign="middle" align="center">อัพเดต 20 พ.ย. 2562</td>
                                        <td valign="middle" align="center"><i class="yicon"><img src="./assets/imgs/ic-view.png" height="12"></i> 1,000</td>
                                        </tr>
                                        
                                        <tr>
                                        <td valign="middle"><a href="read-novel.php">ตอนที่ 22: ลำแสงที่พุ่งมา</a></td>
                                        <td valign="middle">&nbsp;</td>
                                        <td valign="middle"><a class="t-gray" href="javascript:;" data-fancybox="" data-src="#popup-topup-buy"><i class="yicon"><img src="./assets/imgs/ic-large-coin.png" height="20"></i> 20 เหรียญ</a></td>
                                        <td valign="middle" align="center">อัพเดต 20 พ.ย. 2562</td>
                                        <td valign="middle" align="center"><i class="yicon"><img src="./assets/imgs/ic-view.png" height="12"></i> 1,000</td>
                                        </tr>
                                        
                                        <tr>
                                        <td valign="middle"><a href="read-novel.php">ตอนที่ 21: ลำแสงที่พุ่งมา</a></td>
                                        <td valign="middle">&nbsp;</td>
                                        <td valign="middle"><a class="t-gray" href="javascript:;" data-fancybox="" data-src="#popup-topup-buy"><i class="yicon"><img src="./assets/imgs/ic-large-coin.png" height="20"></i> 20 เหรียญ</a></td>
                                        <td valign="middle" align="center">อัพเดต 20 พ.ย. 2562</td>
                                        <td valign="middle" align="center"><i class="yicon"><img src="./assets/imgs/ic-view.png" height="12"></i> 1,000</td>
                                        </tr>

                                        <tr>
                                        <td valign="middle"><a href="read-novel.php">ตอนที่ 20: ลำแสงที่พุ่งมา</a></td>
                                        <td valign="middle">&nbsp;</td>
                                        <td valign="middle"><a class="t-gray" href="javascript:;" data-fancybox="" data-src="#popup-confirm-buy"><i class="yicon"><img src="./assets/imgs/ic-large-coin.png" height="20"></i> 20 เหรียญ</a></td>
                                        <td valign="middle" align="center">อัพเดต 20 พ.ย. 2562</td>
                                        <td valign="middle" align="center"><i class="yicon"><img src="./assets/imgs/ic-view.png" height="12"></i> 1,000</td>
                                        </tr>
                                        
                                        <tr>
                                        <td valign="middle"><a href="read-novel.php">ตอนที่ 19: ลำแสงที่พุ่งมา</a></td>
                                        <td valign="middle">&nbsp;</td>
                                        <td valign="middle"><a class="t-gray" href="javascript:;" data-fancybox="" data-src="#popup-topup-buy"><i class="yicon"><img src="./assets/imgs/ic-large-coin.png" height="20"></i> 20 เหรียญ</a></td>
                                        <td valign="middle" align="center">อัพเดต 20 พ.ย. 2562</td>
                                        <td valign="middle" align="center"><i class="yicon"><img src="./assets/imgs/ic-view.png" height="12"></i> 1,000</td>
                                        </tr>
                                        
                                        <tr>
                                        <td valign="middle"><a href="read-novel.php">ตอนที่ 17: ลำแสงที่พุ่งมา</a></td>
                                        <td valign="middle">&nbsp;</td>
                                        <td valign="middle"><a class="t-gray" href="javascript:;" data-fancybox="" data-src="#popup-topup-buy"><i class="yicon"><img src="./assets/imgs/ic-large-coin.png" height="20"></i> 20 เหรียญ</a></td>
                                        <td valign="middle" align="center">อัพเดต 20 พ.ย. 2562</td>
                                        <td valign="middle" align="center"><i class="yicon"><img src="./assets/imgs/ic-view.png" height="12"></i> 1,000</td>
                                        </tr>

                                        <tr>
                                        <td valign="middle"><a href="read-novel.php">ตอนที่ 9: ลำแสงที่พุ่งมา</a></td>
                                        <td valign="middle">&nbsp;</td>
                                        <td valign="middle"><a class="t-gray" href="javascript:;" data-fancybox="" data-src="#popup-confirm-buy"><i class="yicon"><img src="./assets/imgs/ic-large-coin.png" height="20"></i> 20 เหรียญ</a></td>
                                        <td valign="middle" align="center">อัพเดต 20 พ.ย. 2562</td>
                                        <td valign="middle" align="center"><i class="yicon"><img src="./assets/imgs/ic-view.png" height="12"></i> 1,000</td>
                                        </tr>
                                        
                                        <tr>
                                        <td valign="middle"><a href="read-novel.php">ตอนที่ 8: ลำแสงที่พุ่งมา</a></td>
                                        <td valign="middle">&nbsp;</td>
                                        <td valign="middle"><a class="t-gray" href="javascript:;" data-fancybox="" data-src="#popup-topup-buy"><i class="yicon"><img src="./assets/imgs/ic-large-coin.png" height="20"></i> 20 เหรียญ</a></td>
                                        <td valign="middle" align="center">อัพเดต 20 พ.ย. 2562</td>
                                        <td valign="middle" align="center"><i class="yicon"><img src="./assets/imgs/ic-view.png" height="12"></i> 1,000</td>
                                        </tr>
                                        
                                        <tr>
                                        <td valign="middle"><a href="read-novel.php">ตอนที่ 6: ลำแสงที่พุ่งมา</a></td>
                                        <td valign="middle">&nbsp;</td>
                                        <td valign="middle"><a class="t-gray" href="javascript:;" data-fancybox="" data-src="#popup-topup-buy"><i class="yicon"><img src="./assets/imgs/ic-large-coin.png" height="20"></i> 20 เหรียญ</a></td>
                                        <td valign="middle" align="center">อัพเดต 20 พ.ย. 2562</td>
                                        <td valign="middle" align="center"><i class="yicon"><img src="./assets/imgs/ic-view.png" height="12"></i> 1,000</td>
                                        </tr>

                                        <tr>
                                        <td valign="middle"><a href="read-novel.php">ตอนที่ 7: ลำแสงที่พุ่งมา</a></td>
                                        <td valign="middle">&nbsp;</td>
                                        <td valign="middle"><a class="t-gray" href="javascript:;" data-fancybox="" data-src="#popup-confirm-buy"><i class="yicon"><img src="./assets/imgs/ic-large-coin.png" height="20"></i> 20 เหรียญ</a></td>
                                        <td valign="middle" align="center">อัพเดต 20 พ.ย. 2562</td>
                                        <td valign="middle" align="center"><i class="yicon"><img src="./assets/imgs/ic-view.png" height="12"></i> 1,000</td>
                                        </tr>
                                        
                                        <tr>
                                        <td valign="middle"><a href="read-novel.php">ตอนที่ 36: ลำแสงที่พุ่งมา</a></td>
                                        <td valign="middle">&nbsp;</td>
                                        <td valign="middle"><a class="t-gray" href="javascript:;" data-fancybox="" data-src="#popup-topup-buy"><i class="yicon"><img src="./assets/imgs/ic-large-coin.png" height="20"></i> 20 เหรียญ</a></td>
                                        <td valign="middle" align="center">อัพเดต 20 พ.ย. 2562</td>
                                        <td valign="middle" align="center"><i class="yicon"><img src="./assets/imgs/ic-view.png" height="12"></i> 1,000</td>
                                        </tr>
                                        
                                        <tr>
                                        <td valign="middle"><a href="read-novel.php">ตอนที่ 15: ลำแสงที่พุ่งมา</a></td>
                                        <td valign="middle">&nbsp;</td>
                                        <td valign="middle"><a class="t-gray" href="javascript:;" data-fancybox="" data-src="#popup-topup-buy"><i class="yicon"><img src="./assets/imgs/ic-large-coin.png" height="20"></i> 20 เหรียญ</a></td>
                                        <td valign="middle" align="center">อัพเดต 20 พ.ย. 2562</td>
                                        <td valign="middle" align="center"><i class="yicon"><img src="./assets/imgs/ic-view.png" height="12"></i> 1,000</td>
                                        </tr>

                                        <tr>
                                        <td valign="middle"><a href="read-novel.php">ตอนที่ 14: ลำแสงที่พุ่งมา</a></td>
                                        <td valign="middle">&nbsp;</td>
                                        <td valign="middle"><a class="t-gray" href="javascript:;" data-fancybox="" data-src="#popup-confirm-buy"><i class="yicon"><img src="./assets/imgs/ic-large-coin.png" height="20"></i> 20 เหรียญ</a></td>
                                        <td valign="middle" align="center">อัพเดต 20 พ.ย. 2562</td>
                                        <td valign="middle" align="center"><i class="yicon"><img src="./assets/imgs/ic-view.png" height="12"></i> 1,000</td>
                                        </tr>
                                        
                                        <tr>
                                        <td valign="middle"><a href="read-novel.php">ตอนที่ 26: ลำแสงที่พุ่งมา</a></td>
                                        <td valign="middle">&nbsp;</td>
                                        <td valign="middle"><a class="t-gray" href="javascript:;" data-fancybox="" data-src="#popup-topup-buy"><i class="yicon"><img src="./assets/imgs/ic-large-coin.png" height="20"></i> 20 เหรียญ</a></td>
                                        <td valign="middle" align="center">อัพเดต 20 พ.ย. 2562</td>
                                        <td valign="middle" align="center"><i class="yicon"><img src="./assets/imgs/ic-view.png" height="12"></i> 1,000</td>
                                        </tr>
                                        
                                        <tr>
                                        <td valign="middle"><a href="read-novel.php">ตอนที่ 16: ลำแสงที่พุ่งมา</a></td>
                                        <td valign="middle">&nbsp;</td>
                                        <td valign="middle"><a class="t-gray" href="javascript:;" data-fancybox="" data-src="#popup-topup-buy"><i class="yicon"><img src="./assets/imgs/ic-large-coin.png" height="20"></i> 20 เหรียญ</a></td>
                                        <td valign="middle" align="center">อัพเดต 20 พ.ย. 2562</td>
                                        <td valign="middle" align="center"><i class="yicon"><img src="./assets/imgs/ic-view.png" height="12"></i> 1,000</td>
                                        </tr>
                                        
                                        <tr class="bg-gray">
                                        <td valign="middle"><a href="read-novel.php">ตอนที่ 3: สำแดงพลัง</a></td>
                                        <td valign="middle">&nbsp;</td>
                                        <td valign="middle"><span class="t-red">ซื้อแล้ว</span></td>
                                        <td valign="middle" align="center">อัพเดต 20 พ.ย. 2562</td>
                                        <td valign="middle" align="center"><i class="yicon"><img src="./assets/imgs/ic-view.png" height="12"></i> 1,000</td>
                                        </tr>
                                        
                                        <tr class="bg-gray">
                                        <td valign="middle"><a href="read-novel.php">ตอนที่ 1: อันตรายระหว่างทาง</a></td>
                                        <td valign="middle">อ่านแล้ว</td>
                                        <td valign="middle"><span class="t-blue">อ่านฟรี</span></td>
                                        <td valign="middle" align="center">อัพเดต 20 พ.ย. 2562</td>
                                        <td valign="middle" align="center"><i class="yicon"><img src="./assets/imgs/ic-view.png" height="12"></i> 1,000</td>
                                        </tr>
                                        
                                        <tr class="bg-gray">
                                        <td valign="middle"><a href="read-novel.php">ตอนที่ 2: แขกผู้มีเกียรติ</a></td>
                                        <td valign="middle">&nbsp;</td>
                                        <td valign="middle"><span class="t-blue">อ่านฟรี</span></td>
                                        <td valign="middle" align="center">อัพเดต 20 พ.ย. 2562</td>
                                        <td valign="middle" align="center"><i class="yicon"><img src="./assets/imgs/ic-view.png" height="12"></i> 1,000</td>
                                        </tr>

                                        <tr class="bg-gray">
                                        <td valign="middle"><a href="read-novel.php">ตอนที่ 5: อันตรายระหว่างทาง</a></td>
                                        <td valign="middle">อ่านแล้ว</td>
                                        <td valign="middle"><span class="t-blue">อ่านฟรี</span></td>
                                        <td valign="middle" align="center">อัพเดต 20 พ.ย. 2562</td>
                                        <td valign="middle" align="center"><i class="yicon"><img src="./assets/imgs/ic-view.png" height="12"></i> 1,000</td>
                                        </tr>
                                        
                                        
                                        </tbody>
                                        </table>

                    </div>
                            
                            <div class="ctrl-paging _flex end-sm center-xs middle-xs t-blue">
                                <span class="mr10-xs">1 - 20 row of  60 </span>
                                <a class="ui-btn-mini-white" href="#Prev"><img src="./assets/imgs/ic-prev-page.png" height="12"></a>
                                <a class="ui-btn-mini-white" href="#Next"><img src="./assets/imgs/ic-next-page.png" height="12"></a>
                            </div>
                        
                    
                </div>
                <!-- /EP -->
            </div>
        </article>
        <!-- /content -->

        <div class="bx related space-top">
		    <div class="head-title center-xs start-md">
			<h2 class="h-topic"><a class="t-black" href="cltegory-list.php" title="เรื่องที่เนื้อหาคล้ายกัน">เรื่องที่เนื้อหาคล้ายกัน</a></h2>
			</div>

			
			<div class="slider promotion-book thm-book">
			<?php for($i=1; $i<=15; $i++) { 

				$img_url = "assets/contents/thm-book-05.png";
				$name = "EXE #2 Light Novel";
				$price = "฿70";
				$author = "ผู้การ F";
				switch($i%5)
				{
					case "1": 
								$img_url = "assets/contents/thm-book-01.png";
								$name = "Black Cover บันทึกของกระทิง";
								$price = "฿75";
								$author = "Jonny Onda";
						break;
					case "2": 
								$img_url = "assets/contents/thm-book-02.png";
								$name = "Tokyo Ghoul re (เควส)";
								$price = "฿70";
								$author = "Shin Towada";
						break;
					case "3": 
								$img_url = "assets/contents/thm-book-03.png";
								$name = "ONE PIECE FILM GOLD";
								$price = "฿66.50";
								$author = "Eiichiro Oda";
						break;
					case "4": 
								$img_url = "assets/contents/thm-book-04.png";
								$name = "Executional ตำนานวีรบุรุษโลก(ไม่อยาก)จำ";
								$price = "฿149";
								$author = "Nightberry";
						break;
				}
				?>
				<div>
				<article id="ar<?php echo $i; ?>">
					<div class="in">
						<figure><a href="book-detail.php#" title="<?php echo $name; ?>"><img src="<?php echo $img_url; ?>" alt="<?php echo $name; ?>"></a>
						<!--i class="tag-new">new</i--></figure>
						<div class="detail">
							<h3><a href="#" title="<?php echo $name; ?>"><?php echo $name; ?></a></h3>
							<!-- <p class="author"><?php echo $author; ?></p> 
							<div class="bar-price">
								<span class="price"><b><?php echo $price; ?></b> บาท</span>
								<span class="old">999 บาท</span>
							</div>-->
						</div>
					</div>
				</article>
				</div>
			<?php } ?>
			</div>
        </div>

		</section>
		
		
	</div>	
  </div>

<!-- footer -->
<?php include("incs/footer.html") ?>
<?php include("incs/lightbox.html") ?>
<!-- /footer -->
<!-- js -->
<?php include("incs/js.html") ?>
<script type="text/javascript" src="assets/js/slick.min.js"></script>
<script>
$('.promotion-book').slick({
  dots: false,
  infinite: true,
  arrows: true,
  speed: 300,
  autoplaySpeed: 5000,
  slidesToShow: 5,
  slidesToScroll: 5,
  //variableWidth: true,
  responsive: [
    {
      breakpoint: 800,
      settings: {
	  	//centerPadding: '60px',
        slidesToShow: 3,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 480,
      settings: {
	  	//centerPadding: '40px',
        slidesToShow: 2,
        slidesToScroll: 1
      }
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ]
});

</script>
<!-- /js -->

</body>
</html>
