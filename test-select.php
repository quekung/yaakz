<!DOCTYPE HTML>
<html dir="ltr" lang="th">
<!-- Top Head -->
<head>
<meta charset="utf-8">

<!-- Title -->
<title>Yaakz - Book Ebook online</title>
   
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta charset="UTF-8">
<meta name="keywords" content="yaakz,book,ebook,webnovel,store">
<meta name="description" content="Yaakz คือเว็บแอพพลิเคชั่นร้านค้าออนไลน์ ที่จำหน่ายหนังสือทั้งในรูปแบบ หนังสือเล่ม อีบุ๊ค และนิยายออนไลน์ พบกันเร็วๆนี้ที่นี่">
<meta name="author" content="bcs">


<link href="assets/cs/theme.css" rel="stylesheet" type="text/css" />

<link rel="contents" href="#toc" title="Table Of Contents" />
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 
<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>


<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->


</head>
<!-- /Top Head -->

<body>

<!-- Headbar -->
<?php include("incs/header.html") ?>
<!-- /Headbar -->
<div class="page-category">
   
   <div id="toc">
		<div class="container">
        <div class="adv-option">
        <div class="ctrl-pk">
						<div class="js-select">
							<select id="select-price" name="pk-price" data-placeholder="Select Price">
																								<option value="0" data-description="Thai Bath">0</option>
																<option value="1" data-description="Thai Bath">200</option>
																<option value="2" data-description="Thai Bath">400</option>
																<option value="3" data-description="Thai Bath">600</option>
																<option value="4" data-description="Thai Bath">800</option>
																<option value="5" data-description="Thai Bath">1000</option>
															</select>
						</div>
						<div class="js-select">
							<select id="select-lang" name="pk-lang" data-placeholder="Content Language">
																<option value="1" data-imagesrc="di/ic-en.png"
									data-description="Content Language">English</option>
								<option value="2" data-imagesrc="di/ic-th.png"
									data-description="Content Language">Thai</option>
								<option value="3" data-imagesrc="di/ic-cn.png"
									data-description="Content Language">Chinese</option>
							</select>
						</div>
						<div class="js-select">
							<select id="select-day" name="pk-day" data-placeholder="Select Days">
																<option value="1" data-description="Thai Bath">1 Day</option>
																<option value="2" data-description="Thai Bath">2 Days</option>
																<option value="3" data-description="Thai Bath">3 Days</option>
																<option value="4" data-description="Thai Bath">4 Days</option>
																<option value="5" data-description="Thai Bath">5 Days</option>
																<option value="6" data-description="Thai Bath">6 Days</option>
																<option value="7" data-description="Thai Bath">7 Days</option>
																<option value="8" data-description="Thai Bath">8 Days</option>
																<option value="9" data-description="Thai Bath">9 Days</option>
																<option value="10" data-description="Thai Bath">10 Days</option>
																<option value="11" data-description="Thai Bath">11 Days</option>
																<option value="12" data-description="Thai Bath">12 Days</option>
																<option value="13" data-description="Thai Bath">13 Days</option>
																<option value="14" data-description="Thai Bath">14 Days</option>
																<option value="15" data-description="Thai Bath">15 Days</option>
															</select>
						</div>
						
						</div>
						<ul class="row _flex between-xsh">
							<li>
								<label for="regis_name">ประเภท</label>
			  					
									<select class="select2 f-big bg-wh" id="type-result" data-placeholder="เลือกประเภท">
										<option value="" select="selected">ทุกประเภท</option>
										<option value="0">ต้วเลือกที่ 1</option>
										<option value="1">ต้วเลือกที่ 2</option>
										<option value="2">ต้วเลือกที่ 3</option>
									</select>
								
							</li>
							<li>
								<label for="regis_name">หมวดหมู่</label>
			  					
								<select id="select-lang" name="pk-lang" data-placeholder="Content Language">
																<option value="1" data-imagesrc="di/ic-en.png"
										data-description="Content Language">English</option>
									<option value="2" data-imagesrc="di/ic-th.png"
										data-description="Content Language">Thai</option>
									<option value="3" data-imagesrc="di/ic-cn.png"
										data-description="Content Language">Chinese</option>
								</select>
								
							</li>
							<li>
								<label for="regis_name">สถานะของเรื่อง</label>
			  					
									<select class="select-box" id="status-result">
										<option value="" select="selected">ทุกถานะ</option>
										<option value="0">ต้วเลือกที่ 1</option>
										<option value="1">ต้วเลือกที่ 2</option>
										<option value="2">ต้วเลือกที่ 3</option>
									</select>
								
							</li>
							
						</ul>
					</div>
        </div>
    </div>
</div>
<!-- footer -->

<!-- /footer -->
<!-- js -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
<script>

//Dropdown plugin data
$(document).ready(function() {
    $('#select-price').select2({
    	placeholder: "Select Price",
		minimumResultsForSearch: 0,
		width: 100
	});
	$('#select-lang').select2({
  		placeholder: 'Content Language',
		minimumResultsForSearch: -1,
		width: 150
	});
	$('#select-day').select2({
    	placeholder: "Select Days",
		minimumResultsForSearch: -1,
		width: 100
	});
    $('.select-box').select2({
  		placeholder: 'Select',
		minimumResultsForSearch: -1,
		width: '100%'
	});
});
</script>
</script>
<!-- /js -->

</body>
</html>
