<!DOCTYPE HTML>
<html dir="ltr" lang="th">
<!-- Top Head -->
<?php include("incs/head-top.html") ?>
<!-- /Top Head -->

<body class="page-cart" onload="setTimeout(function () {
			$('.no-result').addClass('hid'); $('.cart-list').removeClass('hid');
		}, 1000);">
<script>
  //<![CDATA[
  $(document).ready(function(){
	  //$('#navigation>ul>li:nth-child(1)>a').addClass('selected');
  });
  //]]>
</script>
<!-- Headbar -->
<?php include("incs/header.html") ?>
<!-- /Headbar -->

   <div id="toc">
		<div class="container">
		<section class="sec-cart">

			<h2 class="h-topic"><span>รายการสินค้า</span></h2>
            <div class="no-result d-flex center-xs middle-xs" onClick="$(this).addClass('hid'); $('.cart-list').removeClass('hid')">
                <img src="./assets/imgs/book-empty.jpg">
            </div>
 
            <div class="cart-list hid">
                <ol>
                    <li class="head">
                        <ul>
                            <li class="c1">รายการ</li>
                            <li class="c2">จำนวน</li>
                            <li class="c3">ราคา</li>
                            <li class="c4"><span class="hid">แก้ไข</span></li>
                        </ul>
                    </li>
                    <?php for($i=1;$i<=3;$i++) { ?> 
                    <li id="or<?php echo $i; ?>">
                        <ul>
                            <li class="c1">
                            <div class="thm-left">
                                <article>
                                    <div class="in">
                                        <figure><a href="#" title="Black Cover บันทึกของกระทิง"><img src="assets/contents/thm-book-0<?php echo $i; ?>.png" alt="Black Cover บันทึกของกระทิง"></a></figure>
                                        <div class="detail">
                                            <h3><a href="#" title="Black Cover บันทึกของกระทิง">Black Cover บันทึกของกระทิง</a></h3>
                                            <div class="type"><i class="yicon"><img src="./assets/imgs/ic-noun-book.png"></i> หนังสือ</div>
                                        </div>
                                    </div>
                                </article>
                            </div>   

                            </li>
                            <li class="c2">1</li>
                            <li class="c3"><div class="t-black"><b class="t-red">259</b> บาท</div></li>
                            <li class="c4"><a class="ui-btn-red btn-sm" href="javascript:;" data-fancybox="" data-src="#popup-delete-cart" data-filter="#or<?php echo $i; ?>" title="ลบรายการสินค้านี้"  onclick="$(this).parents('#or<?php echo $i; ?>').remove();"><i class="yicon"><img src="assets/imgs/ic-trash-white.png" height="12"></i><span class="hidden-xs"> ลบทิ้ง</span></a></li>
                        </ul>
                    </li>    
                    <?php } ?>

                    <li class="total">
                        <ul>
                            <li class="c1">รวมทั้งหมด</li>
                            <li class="c2">3</li>
                            <li class="c3"><div class="t-black"><b class="t-red">777</b> บาท</div></li>
                            <li class="c4">&nbsp;</li>
                        </ul>
                    </li>
                </ol>
                <div class="ctrl-btn d-flex end-xs mt20-xs _chd-cl-xs-06-sm-04-md-03-lg-02-pd0">
                    <span><a href="checkout.php" class="ui-btn-blue btn-md btn-block">ดำเนินการต่อ</a></span>
                </div>
            </div> 
                

		</section>



		</div>
  </div>

<!-- footer -->
<?php include("incs/footer.html") ?>
<?php include("incs/lightbox.html") ?>
<!-- confirm delete-->
<div id="popup-delete-cart" class="thm-popup">
<div class="box-middle">
<div class="inner md-read pa20-xs pa30-md txt-c">
	<i class="icon"><img src="assets/imgs/ic-trash.png" height="80"></i>
	<h2 class="head t-black">ลบรายจากออกจากตระกร้าสินค้า</h2>
	<div class="msg txt-c pa30-xs _flex center-xs">
		<p class="_self-cl-xs-12-md-10"><small>ยืนยันการลบรายการนี้ออกจากตระกร้าสินค้า</small></p>
	</div>
	<p class="mt30-xs d-flex center-xs _chd-cl-xs-06-sm-05-md-04">
		<span><a class="ui-btn-red btn-lg btn-block" href="javascript:;" data-fancybox-close="" onClick="parent.jQuery.fancybox.close(); $('#ar1').remove();">ยืนยัน</a></span></p>
</div>
</div>
</div>
<!-- /confirm delete-->
<!-- /footer -->
<!-- js -->
<?php include("incs/js.html") ?>

<!-- /js -->

</body>
</html>