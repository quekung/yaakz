<!DOCTYPE HTML>
<html dir="ltr" lang="th">
<!-- Top Head -->
<?php include("incs/head-top.html") ?>
<link href="https://fonts.googleapis.com/css?family=Sarabun|Chakra+Petch:300|Taviraj:200" rel="stylesheet">
<!-- /Top Head -->

<body class="page-book">
	<script>
		//<![CDATA[
		$(document).ready(function() {
			$('#navigation>ul>li:nth-child(2)>a').addClass('selected');
		});
		//]]>
	</script>
	<!-- Headbar -->
	<?php include("incs/header.html") ?>
	<!-- /Headbar -->


	<div id="toc" class="pt10">

		<div class="container">
			<div class="crumb"><a href="#">หน้าแรก</a> / <a href="#">นิยาย</a> / <a href="#">นิยายแฟนตาซี</a> / <a href="#">Executional ตำนานวีรบุรุษโลก(ไม่อยาก)จำ</a> / <span>ตอนที่ 1 กินคนได้?</span></div>

			<section class="sec-read-full pt0">
				<div class="box-wh pd0-xs">
					<header class="bar-head-title _flex start-xs">
						<h1>
							<span class="hid">Executional ตำนานวีรบุรุษโลก(ไม่อยาก)จำ</span>
							ตอนที่ 1 : กินคนได้?
						</h1>
					</header>
					<!-- <div class="bar-ctrl-page">
					<div id="fix-bar" class="_flex between-xs">
						<div class="col-l">
							<a href="#" title="กลับหน้าหลัก"><i class="icb-arrow-left"></i><span class="hidden-xs"> กลับหน้าหลัก</span></a>
						</div>
						<div class="mid">
							<h2 class="title txt-c">ตอนที่ 1 : กินคนได้?</h2>
						</div>
						<div class="col-r">
							<a href="#" title="ตอนที่ 2: แขกผู้มีเกียรติ "><span class="hidden-xs">ตอนที่ 2: แขกผู้มีเกียรติ </span><i class="icb-arrow-right"></i></a>
						</div>
					</div>
					</div> -->
					<article class="content">
						<div class="reader body-text">

							<div class="read-body f-sarabun">
								<p>เขานั้นดื้อดึงและไม่ยินยอมที่จะเป็นเพียงคนธรรมดา ทว่าเส้นทางของเขากลับถูกกำหนดให้เป็นเช่นนั้น ถือกำเนิดขึ้นในตระกูลสาขาเล็กจ้อย ทว่าวันหนึ่ง ดวงตาซ้ายของเขาก็ได้หลอมรวมเข้ากับดวงตาของเทพบรรพกาล</p>
								<p>นับแต่นั้น เขาก็ได้แปรเปลี่ยนจากมัจฉาเป็นมังกร เขารุ่งโรจน์ราวดารา เหยียบย่างในเส้นทางของผู้ฝึกตนอันเป็นตำนาน
									จากมดปลวกเล็กจ้อย ณ จุดต่ำสุดของโลกเติบโตขึ้นทีละก้าวไปยังสำนักมากอำนาจ สำนักโบราณอันแข็งแกร่ง เผชิญอัจฉริยะจำนวนนับไม่ถ้วนนี่คือยุคแห่งตำนาน!</p>
								<p>จากคลื่นพลังในร่างชายชราคนนี้ จะเห็นได้ว่าเขาน่าจะมาจากเผ่าพันธุ์ใกล้เคียงกับเซียน มีพลังราวๆ จุดสูงสุดของก้าวที่สอง ฉะนั้นตอนที่เขาเอ่ยถึงระยะทาง เลยใช้คำว่าก้าวที่สามมาเป็นมาตรฐานความเร็ว</p>
								<p>นี่คือมาตรฐานของขั้นพลัง ใช้เจ้าปกครองโลกเป็นแกนกลาง หากขั้นพลังเกินกว่ามาตรฐานนี้ก็จะแบ่งเป็นต้น กลาง ปลาย และสมบูรณ์สี่ระดับ เหนือกว่านั้นจะเป็นยอดผู้ฝึกฌานรองภัยพิบัติตะวันกับภัยพิบัติตะวันจริงๆ</p>
								<p>จากคลื่นพลังในร่างชายชราคนนี้ จะเห็นได้ว่าเขาน่าจะมาจากเผ่าพันธุ์ใกล้เคียงกับเซียน มีพลังราวๆ จุดสูงสุดของก้าวที่สอง ฉะนั้นตอนที่เขาเอ่ยถึงระยะทาง เลยใช้คำว่าก้าวที่สามมาเป็นมาตรฐานความเร็ว</p>
								<p>พอได้ยินชายชราเอ่ย นัยน์ตาซูหมิงขยับประกาย ก่อนพยักหน้าด้วยใบหน้าไร้อารมณ์ แม้เจ็ดแปดสิบคนนี้มีขั้นพลังไม่ธรรมดา แต่ซูหมิงรู้ว่าวิธีการปรากฏตัวของตนน่าตะลึงอย่างยิ่ง ทั้งยังมีสัตว์ร้ายหัวหงส์ที่แผ่กระจายกลิ่นอายพลังน่าสะพรึงกลัวอีก ดังนั้นคนเหล่านี้จึงตื่นตระหนก และเข้าใจผิดคิดว่าเขาเป็นเจ้าปกครองโลกเดินทางมาที่นี่จากนอกอวกาศ</p>
							</div>

						</div>

					</article>
					<div class="bar-ctrl-page ft _flex between-xs">
						<div class="col-l">
							<a href="#" title="ตอนก่อนหน้า"><i class="yicon"><img src="./assets/imgs/nav-prev-gr.png" height="20"></i><span class="hidden-xs"> ตอนก่อนหน้า</span></a>
						</div>
						<div class="col-r">
							<a href="#" title="ตอนที่ 2: แขกผู้มีเกียรติ ">ตอนต่อไป <i class="yicon"><img src="./assets/imgs/nav-next-gr.png" height="20"></i></a>
						</div>
					</div>
					<div class="tools-read">
					<div class="sw-ep">
						<a id="ep-list" class="dropbtn" href="javascript:;" onclick="callEP()">
							<img src="./assets/contents/thm-book-02.png" alt="invoice">
							<div class="info">
								<i class="yicon"><img src="./assets/imgs/ic-angle-up.png" height="9"></i>
								<h2><small class="d-block">Executional</small> ตำนานวีรบุรุษโลก(ไม่อยาก)จำ</h2>
							</div>
						</a>
						<div id="myEP" class="dropdown-content">

							<ul class="show-list">
								<li><a class="active" href="read-novel.php?1" title="ตอนที่ 1: กินคนได้?">
									<h3>ตอนที่ 1: กินคนได้?</h3>
									<span class="t-blue">อ่านฟรี</span>
								</a></li>
								<li><a href="read-novel.php?1" title="ตอนที่ 2: แขกผู้มีเกียรติ">
									<h3>ตอนที่ 2: แขกผู้มีเกียรติ</h3>
									<span class="t-blue">อ่านฟรี</span>
								</a></li>
								<li><a href="read-novel.php?1" title="ตอนที่ 3: xxx">
									<h3>ตอนที่ 3: xxx</h3>
									<span class="t-red">ซื้อแล้ว</span>
								</a></li>
								<li><a href="javascript:;" data-fancybox="" data-src="#popup-topup-buy" title="ตอนที่ 4: xxx">
									<h3>ตอนที่ 4: อันตรายระหว่างทาง  อันตราย...</h3>
									<span class="t-gray">10 เหรียญ</span>
								</a></li>
								<li><a href="javascript:;" data-fancybox="" data-src="#popup-topup-buy" title="ตอนที่ 4: xxx">
									<h3>ตอนที่ 5: อันตรายระหว่างทาง  อันตราย...</h3>
									<span class="t-gray">10 เหรียญ</span>
								</a></li>

								<li><a href="javascript:;" data-fancybox="" data-src="#popup-topup-buy" title="ตอนที่ 4: xxx">
									<h3>ตอนที่ 6: อันตรายระหว่างทาง  อันตราย...</h3>
									<span class="t-gray">10 เหรียญ</span>
								</a></li>
								<li><a href="javascript:;" data-fancybox="" data-src="#popup-topup-buy" title="ตอนที่ 4: xxx">
									<h3>ตอนที่ 7: อันตรายระหว่างทาง  อันตราย...</h3>
									<span class="t-gray">10 เหรียญ</span>
								</a></li>
								<li><a href="javascript:;" data-fancybox="" data-src="#popup-topup-buy" title="ตอนที่ 4: xxx">
									<h3>ตอนที่ 8: อันตรายระหว่างทาง  อันตราย...</h3>
									<span class="t-gray">10 เหรียญ</span>
								</a></li>
								<li><a href="javascript:;" data-fancybox="" data-src="#popup-topup-buy" title="ตอนที่ 4: xxx">
									<h3>ตอนที่ 9: อันตรายระหว่างทาง  อันตราย...</h3>
									<span class="t-gray">10 เหรียญ</span>
								</a></li>
								<li><a href="javascript:;" data-fancybox="" data-src="#popup-topup-buy" title="ตอนที่ 4: xxx">
									<h3>ตอนที่ 10: อันตรายระหว่างทาง  อันตราย...</h3>
									<span class="t-gray">10 เหรียญ</span>
								</a></li>
								
							</ul>
						</div>
					</div>


					<span><a id="bigger" class="btn" href="javascript:;" title="เพิ่มขนาดตัวอักษร"><i class="icb-zoomin"><img src="./assets/imgs/ic-up-font.png" height="24"></i></a></span>
					<span><a id="smaller" class="btn" href="javascript:;" title="ลดขนาดตัวอักษร"><i class="icb-zoomout"><img src="./assets/imgs/ic-down-font.png" height="24"></i></a></span>
					
					<span class="js-select">
						<select class="select2" id="select-style" name="select-style" data-placeholder="เลือก ฟอนต์">
							<option></option>
							<option  id="Sarabun" onclick="normalFont()" value="1">ฟอนต์ Sarabun</option>
							<option id="ChakraPetch" onclick="style2Font()" value="2">ฟอนต์ Chakra Petch</option>
							<option id="Taviraj" onclick="style3Font()" value="3">ฟอนต์ Taviraj</option>
						</select>
					</span>
					
					<span class="js-select">
						<select class="select2" id="select-bg" name="select-bg" data-placeholder="เลือก พื้นหลัง">
							<option></option>
							<option  id="bgNormal" onclick="normalBG()" value="1">พื้นหลัง ปกติ</option>
							<option id="bgSepia" onclick="style2BG()" value="2">พื้นหลัง ซีเปีย</option>
							<option id="bgNight" onclick="style3BG()" value="3">พื้นหลัง กลางคืน</option>
						</select>
					</span>

					<span class="share"><a class="btn" href="javascript:;" data-fancybox="share" data-src="#popup-share" title="แชร์เรื่องนี้"><i class="yicon"><img src="./assets/imgs/ic-share-bar.png" height="14"></i> <small class="d-block">แชร์</small></a></span>
				</div>




				</div>
			</section>


		</div>
	</div>

	<!-- footer -->
	<?php include("incs/footer.html") ?>
	<?php include("incs/lightbox.html") ?>
	<!-- /footer -->
	<!-- js -->
	<?php include("incs/js.html") ?>
	<script src="assets/js/full-reader.js"></script>
	<!-- /js -->

</body>

</html>