<!DOCTYPE HTML>
<html dir="ltr" lang="th">
<!-- Top Head -->
<?php include("incs/head-top.html"); ?>
<!-- /Top Head -->

<body class="page-home">
<!-- header -->
<?php include("incs/header.html"); ?>
<!-- /header -->
<div id="toc" class="wrapper">
	
	
	<section id="hl" class="sec-highlight">
		<div class="container">
			<div class="row _chd-cl-xs-12-md-09">
				<!-- slider -->
				<div class="main-slider slick-slider slick-dotted"  role="toolbar">
					<div class="slider main variable-width">
						<div> <a href="book-detail.php"> <img src="assets/contents/big-slider1.jpg"> </a> </div>
						<div> <a href="book-detail.php"> <img src="assets/contents/big-slider1.jpg"> </a> </div>
						<div> <a href="book-detail.php"> <img src="assets/contents/big-slider1.jpg"> </a> </div>
						<div> <a href="book-detail.php"> <img src="assets/contents/big-slider1.jpg"> </a> </div>
						<div> <a href="book-detail.php"> <img src="assets/contents/big-slider1.jpg"> </a> </div>						
					</div>
				</div>
				<!-- /slider -->
				<aside class="_self-cl-xs-12-md-03">
					<figure>
					<a href="item-detail.php" title="item name">
						<img src="assets/contents/thm_330-247-01.png">
						<span class="ui-btn-red-cr">Shop Now</span>
						<figcaption>item Name</figcaption>
					</a>
					</figure>
					<figure>
					<a href="item-detail.php" title="item name2">
						<img src="assets/contents/thm_330-247-02.png">
						<span class="ui-btn-red-cr">Shop Now</span>
						<figcaption>item Name2</figcaption>
					</a>
					</figure>
				</aside>
			</div>
			<nav class="hl-cat">
				<ul class="row _chd-cl-xs-06-sm-03">
					<li><a href="#" title="แฟนตาซี"><h3>แฟนตาซี</h3><img src="assets/imgs/bg-nav-01.jpg" alt="แฟนตาซี"></a></li>
					<li><a href="#" title="นิยายกำลังภายใน"><h3>นิยายกำลังภายใน</h3><img src="assets/imgs/bg-nav-02.jpg" alt="นิยายกำลังภายใน"></a></li>
					<li><a href="#" title="รัก โรแมนติค"><h3>รัก โรแมนติค</h3><img src="assets/imgs/bg-nav-03.jpg" alt="รัก โรแมนติค"></a></li>
					<li><a href="#" title="คอมมาดี้"><h3>คอมมาดี้</h3><img src="assets/imgs/bg-nav-04.jpg" alt="คอมมาดี้"></a></li>

					<li><a href="#" title="พัฒนาตนเอง"><h3>พัฒนาตนเอง</h3><img src="assets/imgs/bg-nav-05.jpg" alt="พัฒนาตนเอง"></a></li>
					<li><a href="#" title="ดราม่า"><h3>ดราม่า</h3><img src="assets/imgs/bg-nav-06.jpg" alt="ดราม่า"></a></li>
					<li><a href="#" title="สยองขวัญ"><h3>สยองขวัญ</h3><img src="assets/imgs/bg-nav-07.jpg" alt="สยองขวัญ"></a></li>
					<li><a href="#" title="ประวัติศาสตร์"><h3>ประวัติศาสตร์</h3><img src="assets/imgs/bg-nav-08.jpg" alt="ประวัติศาสตร์"></a></li>   
				</ul>
			</nav>
		</div>

	</section>
	
	<section id="promotion" class="sec-promotion">
		<div class="container">
			<h2 class="h-title"><a class="between-xs" href="#" title="โปรโมชั่น"><span class="h-text t-white">โปรโมชั่น</span> <span class="more t-white">ดูทั้งหมด</span></a></h2>
			<div class="slider promotion-book thm-book">
				<div>
				<article>
					<div class="in">
						<figure><a href="#" title="Black Cover บันทึกของกระทิง"><img src="assets/contents/thm-book-01.png" alt="Black Cover บันทึกของกระทิง"></a><i class="tag-new">new</i></figure>
						<div class="detail">
							<h3><a href="#" title="Black Cover บันทึกของกระทิง">Black Cover บันทึกของกระทิง</a></h3>
							<div class="bar-price">
								<span class="price"><b>299</b> บาท</span>
								<span class="old">999 บาท</span>
							</div>
						</div>
					</div>
				</article>
				</div>
				<div>
				<article>
					<div class="in">
						<figure><a href="#" title="Executional ตำนานวีรบุรุษโลก(ไม่อยาก)จำ"><img src="assets/contents/thm-book-02.png" alt="Executional ตำนานวีรบุรุษโลก(ไม่อยาก)จำ"></a><i class="tag-new">new</i></figure>
						<div class="detail">
							<h3><a href="#" title="Executional ตำนานวีรบุรุษโลก(ไม่อยาก)จำ">Executional ตำนานวีรบุรุษโลก(ไม่อยาก)จำ</a></h3>
							<div class="bar-price">
								<span class="price"><b>299</b> บาท</span>
								<span class="old">999 บาท</span>
							</div>
						</div>
					</div>
				</article>
				</div>
				<div>
				<article>
					<div class="in">
						<figure><a href="#" title="Black Cover บันทึกของกระทิง"><img src="assets/contents/thm-book-03.png" alt="Black Cover บันทึกของกระทิง"></a><i class="tag-new">new</i></figure>
						<div class="detail">
							<h3><a href="#" title="Black Cover บันทึกของกระทิง">Black Cover บันทึกของกระทิง</a></h3>
							<div class="bar-price">
								<span class="price"><b>299</b> บาท</span>
								<span class="old">999 บาท</span>
							</div>
						</div>
					</div>
				</article>
				</div>
				<div>
				<article>
					<div class="in">
						<figure><a href="#" title="Executional ตำนานวีรบุรุษโลก(ไม่อยาก)จำ"><img src="assets/contents/thm-book-04.png" alt="Executional ตำนานวีรบุรุษโลก(ไม่อยาก)จำ"></a><i class="tag-new">new</i></figure>
						<div class="detail">
							<h3><a href="#" title="Black Cover บันทึกของกระทิง">Executional ตำนานวีรบุรุษโลก(ไม่อยาก)จำ</a></h3>
							<div class="bar-price">
								<span class="price"><b>299</b> บาท</span>
								<span class="old">999 บาท</span>
							</div>
						</div>
					</div>
				</article>
				</div>
				<div>
				<article>
					<div class="in">
						<figure><a href="#" title="Black Cover บันทึกของกระทิง"><img src="assets/contents/thm-book-05.png" alt="Black Cover บันทึกของกระทิง"></a><i class="tag-new">new</i></figure>
						<div class="detail">
							<h3><a href="#" title="Black Cover บันทึกของกระทิง">Black Cover บันทึกของกระทิง</a></h3>
							<div class="bar-price">
								<span class="price"><b>299</b> บาท</span>
								<span class="old">999 บาท</span>
							</div>
						</div>
					</div>
				</article>
				</div>
				<div>
				<article>
					<div class="in">
						<figure><a href="#" title="Black Cover บันทึกของกระทิง"><img src="assets/contents/thm-book-01.png" alt="Black Cover บันทึกของกระทิง"></a><i class="tag-new">new</i></figure>
						<div class="detail">
							<h3><a href="#" title="Black Cover บันทึกของกระทิง">Black Cover บันทึกของกระทิง</a></h3>
							<div class="bar-price">
								<span class="price"><b>299</b> บาท</span>
								<span class="old">999 บาท</span>
							</div>
						</div>
					</div>
				</article>
				</div>
				<div>
				<article>
					<div class="in">
						<figure><a href="#" title="Executional ตำนานวีรบุรุษโลก(ไม่อยาก)จำ"><img src="assets/contents/thm-book-02.png" alt="Executional ตำนานวีรบุรุษโลก(ไม่อยาก)จำ"></a><i class="tag-new">new</i></figure>
						<div class="detail">
							<h3><a href="#" title="Executional ตำนานวีรบุรุษโลก(ไม่อยาก)จำ">Executional ตำนานวีรบุรุษโลก(ไม่อยาก)จำ</a></h3>
							<div class="bar-price">
								<span class="price"><b>299</b> บาท</span>
								<span class="old">999 บาท</span>
							</div>
						</div>
					</div>
				</article>
				</div>
				<div>
				<article>
					<div class="in">
						<figure><a href="#" title="Black Cover บันทึกของกระทิง"><img src="assets/contents/thm-book-03.png" alt="Black Cover บันทึกของกระทิง"></a><i class="tag-new">new</i></figure>
						<div class="detail">
							<h3><a href="#" title="Black Cover บันทึกของกระทิง">Black Cover บันทึกของกระทิง</a></h3>
							<div class="bar-price">
								<span class="price"><b>299</b> บาท</span>
								<span class="old">999 บาท</span>
							</div>
						</div>
					</div>
				</article>
				</div>
				<div>
				<article>
					<div class="in">
						<figure><a href="#" title="Executional ตำนานวีรบุรุษโลก(ไม่อยาก)จำ"><img src="assets/contents/thm-book-04.png" alt="Executional ตำนานวีรบุรุษโลก(ไม่อยาก)จำ"></a><i class="tag-new">new</i></figure>
						<div class="detail">
							<h3><a href="#" title="Black Cover บันทึกของกระทิง">Executional ตำนานวีรบุรุษโลก(ไม่อยาก)จำ</a></h3>
							<div class="bar-price">
								<span class="price"><b>299</b> บาท</span>
								<span class="old">999 บาท</span>
							</div>
						</div>
					</div>
				</article>
				</div>
				<div>
				<article>
					<div class="in">
						<figure><a href="#" title="Black Cover บันทึกของกระทิง"><img src="assets/contents/thm-book-05.png" alt="Black Cover บันทึกของกระทิง"></a><i class="tag-new">new</i></figure>
						<div class="detail">
							<h3><a href="#" title="Black Cover บันทึกของกระทิง">Black Cover บันทึกของกระทิง</a></h3>
							<div class="bar-price">
								<span class="price"><b>299</b> บาท</span>
								<span class="old">999 บาท</span>
							</div>
						</div>
					</div>
				</article>
				</div>


			</div>
		</div>
	</section>
	
	<section id="ebook" class="sec-ebook">
		<div class="container">
			<div class="bx">
				<h2 class="h-topic"><a class="between-xs" href="cltegory-list.php" title="หนังสือ & อีบุ๊ค">หนังสือ & อีบุ๊ค <span class="more">ดูทั้งหมด</span></a></h2>
				<div class="thm-book d-flex between-xsh">
					<article>
						<div class="in">
							<figure><a href="book-detail.php" title="Black Cover บันทึกของกระทิง"><img src="assets/contents/thm-book-01.png" alt="Black Cover บันทึกของกระทิง"></a><i class="tag-new">new</i></figure>
							<div class="detail">
								<h3><a href="book-detail.php" title="Black Cover บันทึกของกระทิง">Black Cover บันทึกของกระทิง</a></h3>
								<div class="bar-price">
									<span class="price"><b>299</b> บาท</span>
									<span class="old">999 บาท</span>
								</div>
							</div>
						</div>
					</article>
					<article>
						<div class="in">
							<figure><a href="book-detail.php" title="Executional ตำนานวีรบุรุษโลก(ไม่อยาก)จำ"><img src="assets/contents/thm-book-02.png" alt="Executional ตำนานวีรบุรุษโลก(ไม่อยาก)จำ"></a><i class="tag-new">new</i></figure>
							<div class="detail">
								<h3><a href="book-detail.php" title="Executional ตำนานวีรบุรุษโลก(ไม่อยาก)จำ">Executional ตำนานวีรบุรุษโลก(ไม่อยาก)จำ</a></h3>
								<div class="bar-price">
									<span class="price"><b>299</b> บาท</span>
									<span class="old">999 บาท</span>
								</div>
							</div>
						</div>
					</article>
					<article>
						<div class="in">
							<figure><a href="book-detail.php" title="Black Cover บันทึกของกระทิง"><img src="assets/contents/thm-book-03.png" alt="Black Cover บันทึกของกระทิง"></a><i class="tag-new">new</i></figure>
							<div class="detail">
								<h3><a href="book-detail.php" title="Black Cover บันทึกของกระทิง">Black Cover บันทึกของกระทิง</a></h3>
								<div class="bar-price">
									<span class="price"><b>299</b> บาท</span>
									<span class="old">999 บาท</span>
								</div>
							</div>
						</div>
					</article>
					<article>
						<div class="in">
							<figure><a href="book-detail.php" title="Executional ตำนานวีรบุรุษโลก(ไม่อยาก)จำ"><img src="assets/contents/thm-book-04.png" alt="Executional ตำนานวีรบุรุษโลก(ไม่อยาก)จำ"></a><i class="tag-new">new</i></figure>
							<div class="detail">
								<h3><a href="book-detail.php" title="Black Cover บันทึกของกระทิง">Executional ตำนานวีรบุรุษโลก(ไม่อยาก)จำ</a></h3>
								<div class="bar-price">
									<span class="price"><b>299</b> บาท</span>
									<span class="old">999 บาท</span>
								</div>
							</div>
						</div>
					</article>
					<article>
						<div class="in">
							<figure><a href="book-detail.php" title="Black Cover บันทึกของกระทิง"><img src="assets/contents/thm-book-05.png" alt="Black Cover บันทึกของกระทิง"></a><i class="tag-new">new</i></figure>
							<div class="detail">
								<h3><a href="book-detail.php" title="Black Cover บันทึกของกระทิง">Black Cover บันทึกของกระทิง</a></h3>
								<div class="bar-price">
									<span class="price"><b>299</b> บาท</span>
									<span class="old">999 บาท</span>
								</div>
							</div>
						</div>
					</article>
				</div>
			</div>

			<div class="bx">
				<h2 class="h-topic"><a class="between-xs" href="cltegory-list.php" title="นิยายออนไลน์">นิยายออนไลน์ <span class="more">ดูทั้งหมด</span></a></h2>
				<div class="thm-book d-flex between-xsh">
					<article>
						<div class="in">
							<figure><a href="novel-detail.php" title="Black Cover บันทึกของกระทิง"><img src="assets/contents/thm-book-01.png" alt="Black Cover บันทึกของกระทิง"></a><i class="tag-new">new</i></figure>
							<div class="detail">
								<h3><a href="novel-detail.php" title="Black Cover บันทึกของกระทิง">Black Cover บันทึกของกระทิง</a></h3>
								<div class="bar-price">
									<span class="price"><b>299</b> บาท</span>
									<span class="old">999 บาท</span>
								</div>
							</div>
						</div>
					</article>
					<article>
						<div class="in">
							<figure><a href="novel-detail.php" title="Executional ตำนานวีรบุรุษโลก(ไม่อยาก)จำ"><img src="assets/contents/thm-book-02.png" alt="Executional ตำนานวีรบุรุษโลก(ไม่อยาก)จำ"></a><i class="tag-new">new</i></figure>
							<div class="detail">
								<h3><a href="novel-detail.php" title="Executional ตำนานวีรบุรุษโลก(ไม่อยาก)จำ">Executional ตำนานวีรบุรุษโลก(ไม่อยาก)จำ</a></h3>
								<div class="bar-price">
									<span class="price"><b>299</b> บาท</span>
									<span class="old">999 บาท</span>
								</div>
							</div>
						</div>
					</article>
					<article>
						<div class="in">
							<figure><a href="novel-detail.php" title="Black Cover บันทึกของกระทิง"><img src="assets/contents/thm-book-03.png" alt="Black Cover บันทึกของกระทิง"></a><i class="tag-new">new</i></figure>
							<div class="detail">
								<h3><a href="novel-detail.php" title="Black Cover บันทึกของกระทิง">Black Cover บันทึกของกระทิง</a></h3>
								<div class="bar-price">
									<span class="price"><b>299</b> บาท</span>
									<span class="old">999 บาท</span>
								</div>
							</div>
						</div>
					</article>
					<article>
						<div class="in">
							<figure><a href="novel-detail.php" title="Executional ตำนานวีรบุรุษโลก(ไม่อยาก)จำ"><img src="assets/contents/thm-book-04.png" alt="Executional ตำนานวีรบุรุษโลก(ไม่อยาก)จำ"></a><i class="tag-new">new</i></figure>
							<div class="detail">
								<h3><a href="novel-detail.php" title="Black Cover บันทึกของกระทิง">Executional ตำนานวีรบุรุษโลก(ไม่อยาก)จำ</a></h3>
								<div class="bar-price">
									<span class="price"><b>299</b> บาท</span>
									<span class="old">999 บาท</span>
								</div>
							</div>
						</div>
					</article>
					<article>
						<div class="in">
							<figure><a href="novel-detail.php" title="Black Cover บันทึกของกระทิง"><img src="assets/contents/thm-book-05.png" alt="Black Cover บันทึกของกระทิง"></a><i class="tag-new">new</i></figure>
							<div class="detail">
								<h3><a href="novel-detail.php" title="Black Cover บันทึกของกระทิง">Black Cover บันทึกของกระทิง</a></h3>
								<div class="bar-price">
									<span class="price"><b>299</b> บาท</span>
									<span class="old">999 บาท</span>
								</div>
							</div>
						</div>
					</article>
				</div>
			</div>

			<div class="bx">
				<h2 class="h-topic"><a class="between-xs" href="cltegory-list.php" title="แมคกาซีน">หนังสือการ์ตูน แฟนตาซี <span class="more">ดูทั้งหมด</span></a></h2>
				<div class="slider magazine thm-mgz">
					<div>
					<article>
						<a class="in" href="#" title="Kimetsu no Yaiba  ดาบพิฆาตอสูร">
							<figure><img src="assets/contents/thm-mg-01.jpg" alt="Kimetsu no Yaiba  ดาบพิฆาตอสูร"></figure>
							<div class="detail">
								<h3>Kimetsu no Yaiba ดาบพิฆาตอสูร</h3>
								<p>อีกด้านหนึ่งจุดที่ลึกที่สุดของประสาทไร้ขอบเขต  คิบุสึจิ มุซัน ได้เริ่มเคลื่อนไหวแล้ว</p>
							</div>
						</a>
					</article>
					</div>
					<div>
					<article>
						<a class="in" href="#" title="Kimetsu no Yaiba  ดาบพิฆาตอสูร">
							<figure><img src="assets/contents/thm-mg-02.jpg" alt="Kimetsu no Yaiba  ดาบพิฆาตอสูร"></figure>
							<div class="detail">
								<h3>Kimetsu no Yaiba ดาบพิฆาตอสูร</h3>
								<p>อีกด้านหนึ่งจุดที่ลึกที่สุดของประสาทไร้ขอบเขต  คิบุสึจิ มุซัน ได้เริ่มเคลื่อนไหวแล้ว</p>
							</div>
						</a>
					</article>
					</div>
					<div>
					<article>
						<a class="in" href="#" title="Kimetsu no Yaiba  ดาบพิฆาตอสูร">
							<figure><img src="assets/contents/thm-mg-01.jpg" alt="Kimetsu no Yaiba  ดาบพิฆาตอสูร"></figure>
							<div class="detail">
								<h3>Kimetsu no Yaiba ดาบพิฆาตอสูร</h3>
								<p>อีกด้านหนึ่งจุดที่ลึกที่สุดของประสาทไร้ขอบเขต  คิบุสึจิ มุซัน ได้เริ่มเคลื่อนไหวแล้ว</p>
							</div>
						</a>
					</article>
					</div>
					<div>
					<article>
						<a class="in" href="#" title="Kimetsu no Yaiba  ดาบพิฆาตอสูร">
							<figure><img src="assets/contents/thm-mg-02.jpg" alt="Kimetsu no Yaiba  ดาบพิฆาตอสูร"></figure>
							<div class="detail">
								<h3>Kimetsu no Yaiba ดาบพิฆาตอสูร</h3>
								<p>อีกด้านหนึ่งจุดที่ลึกที่สุดของประสาทไร้ขอบเขต  คิบุสึจิ มุซัน ได้เริ่มเคลื่อนไหวแล้ว</p>
							</div>
						</a>
					</article>
					</div>
					
				</div>
			</div>

		</div>
	</section>
	
	<section id="showcase" class="sec-showcase">
		<div class="container">
			<h2 class="h-title txt-c t-white">Product Showcase</h2>
			<div class="d-grid">
				<figure class="g-item g1">
					<a href="assets/contents/showcase_haft_01.jpg" data-fancybox="showcase" data-caption="My caption1">
						<img src="assets/contents/showcase_haft_01.jpg" alt="" />
					</a>
				</figure>
				<figure class="g-item g2">
					<a href="assets/contents/showcase_full_01.jpg" data-fancybox="showcase" data-caption="My caption2">
						<img src="assets/contents/showcase_full_01.jpg" alt="" />
					</a>
				</figure>
				<figure class="g-item g3">
					<a href="assets/contents/showcase_haft_02.jpg" data-fancybox="showcase" data-caption="My caption3">
						<img src="assets/contents/showcase_haft_02.jpg" alt="" />
					</a>
				</figure>
				<figure class="g-item g4">
					<a href="assets/contents/showcase_full_02.jpg" data-fancybox="showcase" data-caption="My caption4">
						<img src="assets/contents/showcase_full_02.jpg" alt="" />
					</a>
				</figure>
				
				<figure class="g-item g5">
					<a href="assets/contents/showcase_full_03.jpg" data-fancybox="showcase" data-caption="My caption5">
						<img src="assets/contents/showcase_full_03.jpg" alt="" />
					</a>
				</figure>
				<figure class="g-item g6">
					<a href="assets/contents/showcase_haft_03.jpg" data-fancybox="showcase" data-caption="My caption6">
						<img src="assets/contents/showcase_haft_03.jpg" alt="" />
					</a>
				</figure>
				<figure class="g-item g7">
					<a href="assets/contents/showcase_full_04.jpg" data-fancybox="showcase" data-caption="My caption7">
						<img src="assets/contents/showcase_full_04.jpg" alt="" />
					</a>
				</figure>
				<figure class="g-item g8">
					<a href="assets/contents/showcase_haft_04.jpg" data-fancybox="showcase" data-caption="My caption8">
						<img src="assets/contents/showcase_haft_04.jpg" alt="" />
					</a>
				</figure>
			</div>
		</div>
	</section>

	<section id="pop-rec" class="sec-popular">
		<div class="container">
			<div class="d-flex between-sm">
				<div class="bx">
					<h2 class="h-topic"><a href="caltegory.php" title="การ์ตูน ยอดนิยม">การ์ตูน ยอดนิยม</a></h2>
					<div class="thm-left">
						<article>
							<div class="in">
								<figure><a href="book-detail.php" title="Black Cover บันทึกของกระทิง"><img src="assets/contents/thm-book-01.png" alt="Black Cover บันทึกของกระทิง"></a></figure>
								<div class="detail">
									<h3><a href="#" title="Black Cover บันทึกของกระทิง">Black Cover บันทึกของกระทิง</a></h3>
									<div class="bar-price">
										<span class="price"><b>299</b> บาท</span>
										<span class="old">999 บาท</span>
									</div>
								</div>
							</div>
						</article>
						<article>
							<div class="in">
								<figure><a href="#" title="Executional ตำนานวีรบุรุษโลก(ไม่อยาก)จำ"><img src="assets/contents/thm-book-02.png" alt="Executional ตำนานวีรบุรุษโลก(ไม่อยาก)จำ"></a></figure>
								<div class="detail">
									<h3><a href="#" title="Executional ตำนานวีรบุรุษโลก(ไม่อยาก)จำ">Executional ตำนานวีรบุรุษโลก(ไม่อยาก)จำ</a></h3>
									<div class="bar-price">
										<span class="price"><b>299</b> บาท</span>
										<span class="old">999 บาท</span>
									</div>
								</div>
							</div>
						</article>
						<article>
							<div class="in">
								<figure><a href="#" title="Black Cover บันทึกของกระทิง"><img src="assets/contents/thm-book-03.png" alt="Black Cover บันทึกของกระทิง"></a></figure>
								<div class="detail">
									<h3><a href="#" title="Black Cover บันทึกของกระทิง">Black Cover บันทึกของกระทิง</a></h3>
									<div class="bar-price">
										<span class="price"><b>299</b> บาท</span>
										<span class="old">999 บาท</span>
									</div>
								</div>
							</div>
						</article>
						<article>
							<div class="in">
								<figure><a href="#" title="Executional ตำนานวีรบุรุษโลก(ไม่อยาก)จำ"><img src="assets/contents/thm-book-04.png" alt="Executional ตำนานวีรบุรุษโลก(ไม่อยาก)จำ"></a></figure>
								<div class="detail">
									<h3><a href="#" title="Black Cover บันทึกของกระทิง">Executional ตำนานวีรบุรุษโลก(ไม่อยาก)จำ</a></h3>
									<div class="bar-price">
										<span class="price"><b>299</b> บาท</span>
										<span class="old">999 บาท</span>
									</div>
								</div>
							</div>
						</article>
						<article>
							<div class="in">
								<figure><a href="#" title="Black Cover บันทึกของกระทิง"><img src="assets/contents/thm-book-05.png" alt="Black Cover บันทึกของกระทิง"></a></figure>
								<div class="detail">
									<h3><a href="#" title="Black Cover บันทึกของกระทิง">Black Cover บันทึกของกระทิง</a></h3>
									<div class="bar-price">
										<span class="price"><b>299</b> บาท</span>
										<span class="old">999 บาท</span>
									</div>
								</div>
							</div>
						</article>
					</div>
				</div>
				
				<div class="bx">
					<h2 class="h-topic"><a href="caltegory.php" title="นิยาย ยอดนิยม">นิยาย ยอดนิยม</a></h2>
					<div class="thm-left">
						<article>
							<div class="in">
								<figure><a href="novel-detail.php" title="Black Cover บันทึกของกระทิง"><img src="assets/contents/thm-book-01.png" alt="Black Cover บันทึกของกระทิง"></a></figure>
								<div class="detail">
									<h3><a href="novel-detail.php" title="Black Cover บันทึกของกระทิง">Black Cover บันทึกของกระทิง</a></h3>
									<div class="bar-price">
										<span class="price"><b>299</b> บาท</span>
										<span class="old">999 บาท</span>
									</div>
								</div>
							</div>
						</article>
						<article>
							<div class="in">
								<figure><a href="novel-detail.php" title="Executional ตำนานวีรบุรุษโลก(ไม่อยาก)จำ"><img src="assets/contents/thm-book-02.png" alt="Executional ตำนานวีรบุรุษโลก(ไม่อยาก)จำ"></a></figure>
								<div class="detail">
									<h3><a href="novel-detail.php" title="Executional ตำนานวีรบุรุษโลก(ไม่อยาก)จำ">Executional ตำนานวีรบุรุษโลก(ไม่อยาก)จำ</a></h3>
									<div class="bar-price">
										<span class="price"><b>299</b> บาท</span>
										<span class="old">999 บาท</span>
									</div>
								</div>
							</div>
						</article>
						<article>
							<div class="in">
								<figure><a href="novel-detail.php" title="Black Cover บันทึกของกระทิง"><img src="assets/contents/thm-book-03.png" alt="Black Cover บันทึกของกระทิง"></a></figure>
								<div class="detail">
									<h3><a href="novel-detail.php" title="Black Cover บันทึกของกระทิง">Black Cover บันทึกของกระทิง</a></h3>
									<div class="bar-price">
										<span class="price"><b>299</b> บาท</span>
										<span class="old">999 บาท</span>
									</div>
								</div>
							</div>
						</article>
						<article>
							<div class="in">
								<figure><a href="novel-detail.php" title="Executional ตำนานวีรบุรุษโลก(ไม่อยาก)จำ"><img src="assets/contents/thm-book-04.png" alt="Executional ตำนานวีรบุรุษโลก(ไม่อยาก)จำ"></a></figure>
								<div class="detail">
									<h3><a href="novel-detail.php" title="Black Cover บันทึกของกระทิง">Executional ตำนานวีรบุรุษโลก(ไม่อยาก)จำ</a></h3>
									<div class="bar-price">
										<span class="price"><b>299</b> บาท</span>
										<span class="old">999 บาท</span>
									</div>
								</div>
							</div>
						</article>
						<article>
							<div class="in">
								<figure><a href="novel-detail.php" title="Black Cover บันทึกของกระทิง"><img src="assets/contents/thm-book-05.png" alt="Black Cover บันทึกของกระทิง"></a></figure>
								<div class="detail">
									<h3><a href="novel-detail.php" title="Black Cover บันทึกของกระทิง">Black Cover บันทึกของกระทิง</a></h3>
									<div class="bar-price">
										<span class="price"><b>299</b> บาท</span>
										<span class="old">999 บาท</span>
									</div>
								</div>
							</div>
						</article>
					</div>
				</div>

				<div class="bx">
					<h2 class="h-topic"><a href="caltegory.php" title="แนะนำ">แนะนำ</a></h2>
					<div class="thm-left">
						<article>
							<div class="in">
								<figure><a href="#" title="Black Cover บันทึกของกระทิง"><img src="assets/contents/thm-book-01.png" alt="Black Cover บันทึกของกระทิง"></a></figure>
								<div class="detail">
									<h3><a href="#" title="Black Cover บันทึกของกระทิง">Black Cover บันทึกของกระทิง</a></h3>
									<div class="bar-price">
										<span class="price"><b>299</b> บาท</span>
										<span class="old">999 บาท</span>
									</div>
								</div>
							</div>
						</article>
						<article>
							<div class="in">
								<figure><a href="#" title="Executional ตำนานวีรบุรุษโลก(ไม่อยาก)จำ"><img src="assets/contents/thm-book-02.png" alt="Executional ตำนานวีรบุรุษโลก(ไม่อยาก)จำ"></a></figure>
								<div class="detail">
									<h3><a href="#" title="Executional ตำนานวีรบุรุษโลก(ไม่อยาก)จำ">Executional ตำนานวีรบุรุษโลก(ไม่อยาก)จำ</a></h3>
									<div class="bar-price">
										<span class="price"><b>299</b> บาท</span>
										<span class="old">999 บาท</span>
									</div>
								</div>
							</div>
						</article>
						<article>
							<div class="in">
								<figure><a href="#" title="Black Cover บันทึกของกระทิง"><img src="assets/contents/thm-book-03.png" alt="Black Cover บันทึกของกระทิง"></a></figure>
								<div class="detail">
									<h3><a href="#" title="Black Cover บันทึกของกระทิง">Black Cover บันทึกของกระทิง</a></h3>
									<div class="bar-price">
										<span class="price"><b>299</b> บาท</span>
										<span class="old">999 บาท</span>
									</div>
								</div>
							</div>
						</article>
						<article>
							<div class="in">
								<figure><a href="#" title="Executional ตำนานวีรบุรุษโลก(ไม่อยาก)จำ"><img src="assets/contents/thm-book-04.png" alt="Executional ตำนานวีรบุรุษโลก(ไม่อยาก)จำ"></a></figure>
								<div class="detail">
									<h3><a href="#" title="Black Cover บันทึกของกระทิง">Executional ตำนานวีรบุรุษโลก(ไม่อยาก)จำ</a></h3>
									<div class="bar-price">
										<span class="price"><b>299</b> บาท</span>
										<span class="old">999 บาท</span>
									</div>
								</div>
							</div>
						</article>
						<article>
							<div class="in">
								<figure><a href="#" title="Black Cover บันทึกของกระทิง"><img src="assets/contents/thm-book-05.png" alt="Black Cover บันทึกของกระทิง"></a></figure>
								<div class="detail">
									<h3><a href="#" title="Black Cover บันทึกของกระทิง">Black Cover บันทึกของกระทิง</a></h3>
									<div class="bar-price">
										<span class="price"><b>299</b> บาท</span>
										<span class="old">999 บาท</span>
									</div>
								</div>
							</div>
						</article>
					</div>
				</div>
			</div>
		</div>
	</section>
		
</div>
<?php include("incs/footer.html"); ?>
<?php include("incs/lightbox.html"); ?>
<!-- Java Script -->
<?php include("incs/js.html"); ?>
<script type="text/javascript" src="assets/js/slick.min.js"></script>
<script>
$('.main').slick({
  dots: true,
  //centerMode: true,
  //centerPadding: '60px',
  autoplay: true,
  autoplaySpeed: 5000,
  slidesToShow: 1,
  variableWidth: false,
  adaptiveHeight: true,
});

$('.promotion-book').slick({
  dots: false,
  infinite: true,
  arrows: true,
  speed: 300,
  autoplaySpeed: 5000,
  slidesToShow: 5,
  slidesToScroll: 5,
  //variableWidth: true,
  responsive: [
    {
      breakpoint: 800,
      settings: {
	  	//centerPadding: '60px',
        slidesToShow: 3,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 480,
      settings: {
	  	//centerPadding: '40px',
        slidesToShow: 2,
        slidesToScroll: 1
      }
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ]
});
$('.magazine').slick({
  dots: false,
  infinite: true,
  arrows: true,
  centerMode: true,
  speed: 300,
  autoplaySpeed: 5000,
  slidesToShow: 2,
  slidesToScroll: 1,
  //variableWidth: true,
  responsive: [
    {
      breakpoint: 480,
      settings: {
	  	//centerPadding: '40px',
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
});
</script>
<!-- /JS -->
</body>
</html>
