<!DOCTYPE HTML>
<html dir="ltr" lang="th">
<!-- Top Head -->
<?php include("incs/head-top.html") ?>
<!-- /Top Head -->

<body class="page-search">
<?php
if ( ! empty($_GET['key-word'])){
	$name =($_GET['key-word']);
}
?>
<script>
  //<![CDATA[
  $(document).ready(function(){
	  //$('#navigation>ul>li:nth-child(1)>a').addClass('selected');
  });
  //]]>
</script>
<!-- Headbar -->
<?php include("incs/header.html") ?>
<!-- /Headbar -->

   <div id="toc">
		<div class="container">

		<div class="z-hd header-srh">

			<form id="advsearch" class="form-search" action="#" method="GET">
				<fieldset class="d-flex row _chd-cl-xs-12 _chd-cl-sm">
					<legend class="hid">ค้นหา</legend>
					<div class="s-keyword">
						<label for="key-word">คำค้นหา</label>
						<input id="key-word" class="txt-box" placeholder="ค้นหาข้อมูล" value="<?php if ( ! empty($_GET['key-word'])){ echo $name; } ?>" name="key-word" type="text" <?php /*?>onfocus="this.value=''"<?php */?>>
						<a href="javascript:;" onClick="$('#key-word').val(''); $('.select-js').select2('val', '0');" class="icn-r"><i class="yicon"><img src="assets/imgs/ic-clear-srh.png"></i></a>
					</div>
					<div class="adv-option _self-cl-sm-07">
						<ul class="row d-flex _chd-cl-xs-12-sm-04-mb10">
							<li>
								<label for="type-result">ประเภท</label>
								
									<select class="select-js" id="type-result" data-placeholder="เลือกประเภท">
										<option value="0" select="selected">ทุกประเภท</option>
										<option value="1">ต้วเลือกที่ 1</option>
										<option value="2">ต้วเลือกที่ 2</option>
										<option value="3">ต้วเลือกที่ 3</option>
									</select>
								
							</li>
							<li>
								<label for="category-result">หมวดหมู่</label>
								
								<select class="select-js" id="category-result" data-placeholder="เลือกหมวดหมู่">
										<!-- <option value=""></option> -->
										<option value="0" select="selected">ทุกหมวดหมู่</option>
										<option value="1">ต้วเลือกที่ 1</option>
										<option value="2">ต้วเลือกที่ 2</option>
										<option value="3">ต้วเลือกที่ 3</option>
									</select>
								
							</li>
							<li>
								<label for="status-result">สถานะของเรื่อง</label>
								
									<select class="select-js" id="status-result" data-placeholder="เลือกสถานะ">
										<option value="0" select="selected">ทุกสถานะ</option>
										<option value="1">ต้วเลือกที่ 1</option>
										<option value="2">ต้วเลือกที่ 2</option>
										<option value="3">ต้วเลือกที่ 3</option>
									</select>
								
							</li>
							
						</ul>
					</div>
				</fieldset>
			</form>
		</div>
		
		<section class="sec-01 pt0">
			<div class="bx">
				<h2 class="h-topic"><span>ผลลัพธ์การค้นหา</span></h2>
				<div class="thm-book row _chd-cl-xs-06-xsh-04-sm-03-md-25 start-xs">
					<?php for($i=1; $i<=12; $i++) { 

						$img_url = "assets/contents/thm-book-05.png";
						$name = "EXE #2 Light Novel";
						$price = "฿70";
						$author = "ผู้การ F";
						switch($i%5)
						{
							case "1": 
										$img_url = "assets/contents/thm-book-01.png";
										$name = "Black Cover บันทึกของกระทิง";
										$price = "฿75";
										$author = "Jonny Onda";
								break;
							case "2": 
										$img_url = "assets/contents/thm-book-02.png";
										$name = "Tokyo Ghoul re (เควส)";
										$price = "฿70";
										$author = "Shin Towada";
								break;
							case "3": 
										$img_url = "assets/contents/thm-book-03.png";
										$name = "ONE PIECE FILM GOLD";
										$price = "฿66.50";
										$author = "Eiichiro Oda";
								break;
							case "4": 
										$img_url = "assets/contents/thm-book-04.png";
										$name = "Executional ตำนานวีรบุรุษโลก(ไม่อยาก)จำ";
										$price = "฿149";
										$author = "Nightberry";
								break;
						}
						?>
						<article>
							<div class="in">
								<figure><a href="#" title="<?php echo $name; ?>"><img src="<?php echo $img_url; ?>" alt="<?php echo $name; ?>"></a><!--i class="tag-new">new</i--></figure>
								<div class="detail">
									<h3><a href="#" title="<?php echo $name; ?>"><?php echo $name; ?></a></h3>
									<p class="author"><?php echo $author; ?></p>
									<!-- <div class="bar-price">
										<span class="price"><b>299</b> บาท</span>
										<span class="old">999 บาท</span>
									</div> -->
								</div>
							</div>
						</article>
						
					<?php } ?>
				</div>
			</div>
			
			<div class="ctrl-btn mt30-md mt10-xs txt-c">
				<a class="more ui-btn-border btn-md" href="#" title="ดูเพิ่มเติม">ดูเพิ่มเติม</a>
			</div>
		</section>



		</div>
  </div>

<!-- footer -->
<?php include("incs/footer.html") ?>
<?php include("incs/lightbox.html") ?>
<!-- /footer -->
<!-- js -->
<?php include("incs/js.html") ?>
<!-- /js -->

</body>
</html>