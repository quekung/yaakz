var docCookies = {
  getItem: function (sKey) {
    if (!sKey) { return null; }
    return decodeURIComponent(document.cookie.replace(new RegExp("(?:(?:^|.*;)\\s*" + encodeURIComponent(sKey).replace(/[\-\.\+\*]/g, "\\$&") + "\\s*\\=\\s*([^;]*).*$)|^.*$"), "$1")) || null;
  },
  setItem: function (sKey, sValue, vEnd, sPath, sDomain, bSecure) {
    if (!sKey || /^(?:expires|max\-age|path|domain|secure)$/i.test(sKey)) { return false; }
    var sExpires = "";
    if (vEnd) {
      switch (vEnd.constructor) {
        case Number:
          sExpires = vEnd === Infinity ? "; expires=Fri, 31 Dec 9999 23:59:59 GMT" : "; max-age=" + vEnd;
          break;
        case String:
          sExpires = "; expires=" + vEnd;
          break;
        case Date:
          sExpires = "; expires=" + vEnd.toUTCString();
          break;
      }
    }
    document.cookie = encodeURIComponent(sKey) + "=" + encodeURIComponent(sValue) + sExpires + (sDomain ? "; domain=" + sDomain : "") + (sPath ? "; path=" + sPath : "") + (bSecure ? "; secure" : "");
    return true;
  },
  removeItem: function (sKey, sPath, sDomain) {
    if (!this.hasItem(sKey)) { return false; }
    document.cookie = encodeURIComponent(sKey) + "=; expires=Thu, 01 Jan 1970 00:00:00 GMT" + (sDomain ? "; domain=" + sDomain : "") + (sPath ? "; path=" + sPath : "");
    return true;
  },
  hasItem: function (sKey) {
    if (!sKey) { return false; }
    return (new RegExp("(?:^|;\\s*)" + encodeURIComponent(sKey).replace(/[\-\.\+\*]/g, "\\$&") + "\\s*\\=")).test(document.cookie);
  },
  keys: function () {
    var aKeys = document.cookie.replace(/((?:^|\s*;)[^\=]+)(?=;|$)|^\s*|\s*(?:\=[^;]*)?(?:\1|$)/g, "").split(/\s*(?:\=[^;]*)?;\s*/);
    for (var nLen = aKeys.length, nIdx = 0; nIdx < nLen; nIdx++) { aKeys[nIdx] = decodeURIComponent(aKeys[nIdx]); }
    return aKeys;
  }
};

/*ctrl fontsize*/

function change_size(element, size) {
    var new_size;
    var current = parseInt(docCookies.getItem("FontSize"));
    if (current !== "") {
        current = parseInt(element.css('font-size'));
    }
    if (size === 'smaller') {
        if (current > 12) {
            new_size = current - 2;
        } else {
        		new_size = current;
		}
    } else if (size === 'bigger') {
        if (current < 32) {
            new_size = current + 2;
        } else {
        		new_size = current;
		}
    }
    
    element.css('font-size', new_size + 'px');
    docCookies.setItem("FontSize", new_size, Infinity);
}

$('#smaller').click(function () {
    change_size($('.read-body p'), 'smaller');
})

$('#bigger').click(function () {
    change_size($('.read-body p'), 'bigger');
});
var fontSize = docCookies.getItem("FontSize");
if (fontSize) {
    $('.read-body p').css('font-size', fontSize + 'px');
}

/*font style*/
//this should be in global scope
var Cookie = parseInt(docCookies.getItem("FontStyle"));
$(document).ready(function () {

    //font family
    var fontstyle = docCookies.getItem("FontStyle") || 'normal';

    if (fontstyle == "normal") {
        $(".read-body p").css({
            "font-family": "Sarabun"
        });
		$("#Sarabun").addClass("active").attr('selected','selected');
    } else if (fontstyle == "style2") {
        $(".read-body p").css({
            "font-family": "Chakra Petch"
        });
		$("#ChakraPetch").addClass("active").attr('selected','selected');
    } else {
        $(".read-body p").css({
            "font-family": "Taviraj"
        });
		$("#Taviraj").addClass("active").attr('selected','selected');
    }
	
	 //bg style
    var bgstyle = docCookies.getItem("bgStyle") || 'normal';

    if (bgstyle == "normal") {
        $("body").addClass('bg-normal');
		$("#bgNormal").addClass("active").attr('selected','selected');
	} else if (bgstyle == "style2") {
		$("body").addClass('bg-sepia');
		$("#bgSepia").addClass("active").attr('selected','selected');;
	} else {
		$("body").addClass('bg-night');
		$("#bgNight").addClass("active").attr('selected','selected');;
	}
	
	/*select load*/
	$('.select2').select2({
    	placeholder: "Please select",
    	//allowClear: true,
		minimumResultsForSearch: -1,
		dropdownAutoWidth : true,
		width: '100%'
	});
}); 

function normalFont() {
    $(".read-body p").css({
        "font-family": "Sarabun"
    });
	//$(".btn-style").removeClass("active");
	$("#Sarabun").addClass("active");
    //string literal
    //fontstyle = 'normal';
    //$.cookie(Cookie, fontstyle);
	docCookies.setItem("FontStyle", "normal", Infinity);
}

function style2Font() {
    $(".read-body p").css({
        "font-family": "Chakra Petch"
    });
	//$(".btn-style").removeClass("active");
	$("#ChakraPetch").addClass("active");
    //string literal
    //fontstyle = 'larger';
    //$.cookie(Cookie, fontstyle);
	docCookies.setItem("FontStyle", "style2", Infinity);
}

function style3Font() {
    $(".read-body p").css({
        "font-family": "Taviraj"
    });
	//$(".btn-style").removeClass("active");
	$("#Taviraj").addClass("active");
    //string literal
    //fontstyle = 'biggest';
    //$.cookie(Cookie, fontstyle);
	docCookies.setItem("FontStyle", "style3", Infinity);
}

function normalBG() {
	$("body").removeClass("bg-sepia");
	$("body").removeClass("bg-night");
    $("body").addClass('bg-normal');
	docCookies.setItem("bgStyle", "normal", Infinity);
}

function style2BG() {
	$("body").removeClass("bg-normal");
	$("body").removeClass("bg-night");
    $("body").addClass('bg-sepia');
	docCookies.setItem("bgStyle", "style2", Infinity);
}

function style3BG() {
	$("body").removeClass("bg-normal");
	$("body").removeClass("bg-sepia");
    $("body").addClass('bg-night');
	docCookies.setItem("bgStyle", "style3", Infinity);
}

/*check change select*/
$('#select-style').on('change', function() {
	if ($(this)[0].selectedIndex == 1) { 
		normalFont();
	} else if ($(this)[0].selectedIndex == 2) {
		style2Font(); 
	} else if ($(this)[0].selectedIndex == 3) {
		style3Font(); 
	}
});
$('#select-bg').on('change', function() {
	if ($(this)[0].selectedIndex == 1) { 
		normalBG();
	} else if ($(this)[0].selectedIndex == 2) {
		style2BG(); 
	} else if ($(this)[0].selectedIndex == 3) {
		style3BG(); 
	}
});

/* episode toggle*/
function callEP() {
    document.getElementById("myEP").classList.toggle("show");
    document.getElementById("ep-wrap").classList.toggle("active");
  }
  // Close the dropdown if the user clicks outside of it
  window.onclick = function(event) {
    if (!event.target.matches('.dropbtn') && !event.target.matches('.dropbtn *') && !event.target.matches('.dropdown-content *') && !event.target.matches('.btn-del')) {
      var dropdowns = document.getElementsByClassName("dropdown-content");
      var i;
      for (i = 0; i < dropdowns.length; i++) {
        var openDropdown = dropdowns[i];
        if (openDropdown.classList.contains('show')) {
          openDropdown.classList.remove('show');
          document.getElementById("ep-list").classList.remove("active");
        }
      }
    }
  }
