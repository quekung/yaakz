<!DOCTYPE HTML>
<html dir="ltr" lang="th">
<!-- Top Head -->
<?php include("incs/head-top.html") ?>
<!-- /Top Head -->

<body class="page-book">
<script>
  //<![CDATA[
  $(document).ready(function(){
	  $('#navigation>ul>li:nth-child(2)>a').addClass('selected');
  });
  //]]>
</script>
<!-- Headbar -->
<?php include("incs/header.html") ?>
<!-- /Headbar -->

   
<div id="toc" class="pt10">
	
	<div class="container">
        <div class="crumb"><a href="#">หน้าแรก</a> / <a href="#">นิยาย</a> / <a href="#">นิยายรักโรมานติก</a> / <span>Kimetsu no Yaiba ดาบพิฆาตอสูร เล่ม 11</span></div>
		<section class="sec-main-book">
        <!-- content -->
        <article class="content">
				
            <div class="head-book">

                <div class="thm">
                    <!-- <figure>
                        <img src="./assets/contents/thm-book-01.png" alt="Kimetsu no Yaiba ดาบพิฆาตอสูร เล่ม 11">
                    </figure> -->
					<div class="book-slider">
						<div class="slider slider-for">
							<div>
							<a data-fancybox="gr" data-caption="Kimetsu no Yaiba ดาบพิฆาตอสูร เล่ม 11" href="./assets/contents/thm-book-01.png"><img src="./assets/contents/thm-book-01.png" alt="Kimetsu no Yaiba ดาบพิฆาตอสูร เล่ม 11"></a>
							</div>
							<div>
							<a data-fancybox="gr" data-caption="Kimetsu no Yaiba ดาบพิฆาตอสูร เล่ม 11" href="./assets/contents/thm-book-02.png"><img src="./assets/contents/thm-book-02.png" alt="Kimetsu no Yaiba ดาบพิฆาตอสูร เล่ม 11"></a>
							</div>
							<div>
							<a data-fancybox="gr" data-caption="Kimetsu no Yaiba ดาบพิฆาตอสูร เล่ม 11" href="./assets/contents/thm-book-03.png"><img src="./assets/contents/thm-book-03.png" alt="Kimetsu no Yaiba ดาบพิฆาตอสูร เล่ม 11"></a>
							</div>
							<div>
							<a data-fancybox="gr" data-caption="Kimetsu no Yaiba ดาบพิฆาตอสูร เล่ม 11" href="./assets/contents/thm-book-04.png"><img src="./assets/contents/thm-book-04.png" alt="Kimetsu no Yaiba ดาบพิฆาตอสูร เล่ม 11"></a>
							</div>
							<div>
							<a data-fancybox="gr" data-caption="Kimetsu no Yaiba ดาบพิฆาตอสูร เล่ม 11" href="./assets/contents/thm-book-05.png"><img src="./assets/contents/thm-book-05.png" alt="Kimetsu no Yaiba ดาบพิฆาตอสูร เล่ม 11"></a>
							</div>
						</div>
						<div class="slider slider-nav">
							<div>
							<img src="./assets/contents/thm-book-01.png" alt="Kimetsu no Yaiba ดาบพิฆาตอสูร เล่ม 11">
							</div>
							<div>
							<img src="./assets/contents/thm-book-02.png" alt="Kimetsu no Yaiba ดาบพิฆาตอสูร เล่ม 11">
							</div>
							<div>
							<img src="./assets/contents/thm-book-03.png" alt="Kimetsu no Yaiba ดาบพิฆาตอสูร เล่ม 11">
							</div>
							<div>
							<img src="./assets/contents/thm-book-04.png" alt="Kimetsu no Yaiba ดาบพิฆาตอสูร เล่ม 11">
							</div>
							<div>
							<img src="./assets/contents/thm-book-05.png" alt="Kimetsu no Yaiba ดาบพิฆาตอสูร เล่ม 11">
							</div>
						</div>
					</div>
                    <ul class="book-author mt30-xs">
                        <li class="_chd-cl-xs"><em class="_self-cl-xs-05-md-04 txt-r">ผู้เขียน :</em>  <a href="#" class="t-blue">Koyoharu Gotouge</a></li>
                        <li class="_chd-cl-xs"><em class="_self-cl-xs-05-md-04 txt-r">หมวดหมู่ :</em> <span>-</span></li>
						<li class="_chd-cl-xs"><em class="_self-cl-xs-05-md-04 txt-r">ผู้แปล :</em> <span>-</span></li>
						<li class="_chd-cl-xs"><em class="_self-cl-xs-05-md-04 txt-r">แปลจากหนังสือ :</em> <span>-</span></li>
						<li class="_chd-cl-xs"><em class="_self-cl-xs-05-md-04 txt-r">ออกแบบปก :</em> <span>SMM PLUS</span></li>
						<li class="_chd-cl-xs"><em class="_self-cl-xs-05-md-04 txt-r">สำนักพิมพ์ :</em> <span>270 หน้า</span></li>
						<li class="_chd-cl-xs"><em class="_self-cl-xs-05-md-04 txt-r">จำนวนหน้า :</em> <span>12.8 x 18.5 x 1.2 CM</span></li>
						<li class="_chd-cl-xs"><em class="_self-cl-xs-05-md-04 txt-r">พิมพ์ครั้งที่ :</em> <span>1 (กรกฎาคม 2563)</span></li>
						<li class="_chd-cl-xs"><em class="_self-cl-xs-05-md-04 txt-r">หมวดหมู่ :</em>  <a href="#" class="t-blue">นิยายแฟนตาซี</a></li>
						<li class="_chd-cl-xs"><em class="_self-cl-xs-05-md-04 txt-r">ISBN :</em> <span>556789222356</span></li>
                    </ul>
                </div>
                <div class="detail">
                    <h1>Kimetsu no Yaiba ดาบพิฆาตอสูร เล่ม 11</h1>
                    <div class="ctrl-btn _flex start-xs _chd-mr30">
						<a class="ui-btn-red" href="read-ebook.php" title="ทดลองอ่าน"><i class="yicon invert"><img src="./assets/imgs/ic-noun-ebook.png" height="24"></i> ทดลองอ่าน</a>
                        <a class="ui-btn-border-gray" href="javascript:;" onClick="$(this).toggleClass('active')" title="เพิ่มรายการที่ชื่นชอบ"><i class="yicon fav"><img src="./assets/imgs/my-favorite.png" height="16"></i> เพิ่มรายการที่ชื่นชอบ</a>
                        <a class="ui-btn-border-gray" href="javascript:;" data-fancybox="share" data-src="#popup-share" title="แชร์เรื่องนี้"><i class="yicon"><img src="./assets/imgs/ic-share.png" height="16"></i> แชร์เรื่องนี้</a>
                        
                        <!--<a class="ui-btn-blue-sq" href="full-read-ep.php" title="ทดลองอ่าน"><i class="ic-view-wh"></i> ทดลองอ่าน</a>-->
                    </div>

                    <div class="type-cart d-flex center-xs start-xsh top-xs">
                        <div class="t1 d-flex flex-column center-xs mr20-xs">
                            <div class="tags f-bold"> <i class="yicon d-inline mr5-xs"><img src="./assets/imgs/ic-noun-book.png" height="24"></i>หนังสือ</div>
                            <div class="price f-big _self-mv10"><b class="t-red">259</b> บาท</div>
                            <div class="add-cart">
								<a href="javascript:;" class="ui-btn-cart btn-md btn-block mb20-xs"><i class="yicon"><img src="./assets/imgs/ic-cart-wh.png" height="20"></i> <small>เพิ่มลงตระกร้าสินค้า</small></a>
								<a class="call-book-set ui-btn-dark btn-md btn-block txt-l _self-pl10" href="javascript:;" data-fancybox="" data-src="#popup-select-book" title="เลือกเล่มอื่นๆ"><i class="yicon"><img src="./assets/imgs/ic-select-book.png" height="20"></i> <small>เลือกเล่มอื่นๆ</small></a>
							</div>
                        </div>

						<div class="t2 d-flex flex-column center-xs">
                            <div class="tags f-bold"> <i class="yicon d-inline mr5-xs"><img src="./assets/imgs/ic-noun-ebook.png" height="24"></i>อีบุ๊ค</div>
                            <div class="price f-big _self-mv10"><b class="t-red">259</b> บาท</div>
                            <div class="add-cart">
								<a href="javascript:;" class="ui-btn-cart btn-md btn-block mb20-xs"><i class="yicon"><img src="./assets/imgs/ic-cart-wh.png" height="20"></i> <small>เพิ่มลงตระกร้าสินค้า</small></a>
								<a class="call-book-set ui-btn-dark btn-md btn-block txt-l" href="javascript:;" data-fancybox="" data-src="#popup-select-book" title="เลือกเล่มอื่นๆ"><i class="yicon"><img src="./assets/imgs/ic-select-book.png" height="20"></i> <small>เลือกเล่มอื่นๆ</small></a>
							</div>
                        </div>

                    </div>

<!-- popup select book-->
<div id="popup-select-book" class="thm-popup">
<div class="wrap-bk">
	<header>
		<h2 class="h-topic"><i class="yicon"><img src="assets/imgs/ic-noun-book.png" height="20"></i>หนังสือ</h2>
		<p>Kimetsu no Yaiba ดาบพิฆาตอสูร  (จำนวน 45 เล่ม)</p>
	</header>
	<div class="accordion">
	<?php $k=0; for($row=1;$row<=5;$row++) { ?>
	<div class="bx">
		<h3 class="acc-h">Kimetsu no Yaiba ดาบพิฆาตอสูร เล่ม  <?php echo $k+1 ?>-<?php echo $k+10 ?><i class="yicon arrow"><img src="assets/imgs/ic-angle-down-red.png" height="10"></i></h3>
		<div class="pane">
			<ul class="mz-chk">
			<?php for($i=1;$i<=10;$i++){ 
				++$k; if($k<=45) { ?> 	
				<li><input class="chd-chk" type="checkbox" value="<?php echo $k; ?>" id="bk<?php echo $k; ?>" name="bk<?php echo $k; ?>"> <label class="ml10-xs" for="bk<?php echo $k; ?>">Kimetsu no Yaiba ดาบพิฆาตอสูร เล่ม <?php echo $k; ?></label></li>
			<?php } } ?> 
			</ul>
		</div>
	</div>
	<?php } ?>
	<!-- <div class="bx">
		<h3 class="acc-h">xxx</h3>
		<div class="pane">test</div>
	</div>
	<div class="bx">
		<h3 class="acc-h">xxx</h3>
		<div class="pane">test</div>
	</div>
	<div class="bx">
		<h3 class="acc-h">xxx</h3>
		<div class="pane">test</div>
	</div> -->
	</div>
	<footer class="d-flex between-xs middle-xs">
		<div class="master mz-chk">
			<input type="checkbox" value="all" id="chk-all" name="chk-all" class="master-chk">
			<label class="ml10-xs" for="chk-all">เลือกทั้งหมด</label>
		</div>
		<span class="_self-cl-xs-05-sm-04-md-03-pd0"><a href="javascript:;" data-fancybox-close="" onClick="parent.jQuery.fancybox.close();" class="ui-btn-blue btn-md btn-block">ตกลง</a></span>
	</footer>
	<script>
		$('.master-chk').click(function() {
			if($(this).is(':checked')) {
				$("input[class*='chd-chk']").prop("checked", true);
				$(this).prop("checked", true);
			} else {
				$("input[class*='chd-chk']").prop("checked", false);
				$(this).prop("checked", false);
			}
		});
		$("input[class*='chd-chk']").change( function() {
			$('.master-chk').prop("checked", false);
		});
	</script>
</div>
</div>
<!-- /popup select book-->

                    <div class="abstract-body mt30-xs">
						<figure class="mb30-xs">
                            <a data-fancybox="images" data-caption="Kimetsu no Yaiba ดาบพิฆาตอสูร เล่ม 11" href="./assets/contents/cover-reader.jpg"><img src="./assets/contents/cover-reader.jpg"></a>
                        </figure>
                        <p class="desc">ทันจิโร่ เป็นเด็กหนุ่มผู้มีจิตใจอ่อนโยนและเฉลียวฉลาด ในฐานะลูกชายคนโตของครอบครัวคนเผาถ่าน หัวเรี่ยวหัวแรงในการหารายได้เลี้ยงดูครอบครัวจากการเผาถ่านไปขายหลังจากพ่อของเขาเสียชีวิต ทุกสิ่งทุกอย่างได้เปลี่ยนแปลงไปเมื่อมีอสูรบุกเข้ามาสังหารคนในครอบครัวของทันจิโร่ เหลือแต่เพียงเขาและน้องสาวที่ชื่อ เนะซึโกะ เท่านั้น เนะซึโกะที่รอดตายมาได้กลับกลายเป็นอสูรไป ทว่าก็น่าประหลาดใจที่เธอยังคงมีความคิดและการแสดงอารมณ์อย่างมนุษย์หลงเหลืออยู่ หลังจากที่ทันจิโร่ได้ต่อสู้กับโทมิโอะกะ กิยู นักล่าอสูรที่เดินทางผ่านมาและต้องการจะฆ่าเนะซึโกะที่กลายเป็นอสูรไปแล้ว เขาจึงตัดสินใจที่จะเป็นนักล่าอสูรตามคำชักชวนของกิยู เพื่อหาทางทำให้น้องสาวกลับมาเป็นมนุษย์ และชำระแค้นจากอสูรที่ฆ่าคนในครอบครัวของเขาให้จงได้</p>
                    </div>
                    

                </div>

            </div>
            
            <div class="reader body-text">

                <!-- <div class="abstract-body">
                    <b class="hid">เรื่องย่อ</b>
                    <p>นับแต่นั้น เขาก็ได้แปรเปลี่ยนจากมัจฉาเป็นมังกร  เขารุ่งโรจน์ราวดารา เหยียบย่างในเส้นทางของผู้ฝึกตนอันเป็นตำนาน จากมดปลวกเล็กจ้อย ณ จุดต่ำสุดของโลกเติบโตขึ้นทีละก้าวไปยังสำนักมากอำนาจ สำนักโบราณอันแข็งแกร่ง เผชิญอัจฉริยะจำนวนนับไม่ถ้วนนี่คือยุคแห่งตำนาน!</p>
                </div> -->
                
                

            </div>
        </article>
        <!-- /content -->

        <div class="bx related space-top">
		    <div class="head-title center-xs start-md">
			<h2 class="h-topic"><a class="t-black" href="cltegory-list.php" title="เรื่องที่เนื้อหาคล้ายกัน">เรื่องที่เนื้อหาคล้ายกัน</a></h2>
			</div>

			
			<div class="slider promotion-book thm-book">
			<?php for($i=1; $i<=15; $i++) { 

				$img_url = "assets/contents/thm-book-05.png";
				$name = "EXE #2 Light Novel";
				$price = "฿70";
				$author = "ผู้การ F";
				switch($i%5)
				{
					case "1": 
								$img_url = "assets/contents/thm-book-01.png";
								$name = "Black Cover บันทึกของกระทิง";
								$price = "฿75";
								$author = "Jonny Onda";
						break;
					case "2": 
								$img_url = "assets/contents/thm-book-02.png";
								$name = "Tokyo Ghoul re (เควส)";
								$price = "฿70";
								$author = "Shin Towada";
						break;
					case "3": 
								$img_url = "assets/contents/thm-book-03.png";
								$name = "ONE PIECE FILM GOLD";
								$price = "฿66.50";
								$author = "Eiichiro Oda";
						break;
					case "4": 
								$img_url = "assets/contents/thm-book-04.png";
								$name = "Executional ตำนานวีรบุรุษโลก(ไม่อยาก)จำ";
								$price = "฿149";
								$author = "Nightberry";
						break;
				}
				?>
				<div>
				<article id="ar<?php echo $i; ?>">
					<div class="in">
						<figure><a href="book-detail.php#" title="<?php echo $name; ?>"><img src="<?php echo $img_url; ?>" alt="<?php echo $name; ?>"></a>
						<!--i class="tag-new">new</i--></figure>
						<div class="detail">
							<h3><a href="#" title="<?php echo $name; ?>"><?php echo $name; ?></a></h3>
							<!-- <p class="author"><?php echo $author; ?></p> 
							<div class="bar-price">
								<span class="price"><b><?php echo $price; ?></b> บาท</span>
								<span class="old">999 บาท</span>
							</div>-->
						</div>
					</div>
				</article>
				</div>
			<?php } ?>
			</div>
        </div>

		</section>
		
		
	</div>	
  </div>

<!-- footer -->
<?php include("incs/footer.html") ?>
<?php include("incs/lightbox.html") ?>
<!-- /footer -->
<!-- js -->
<?php include("incs/js.html") ?>
<script type="text/javascript" src="assets/js/slick.min.js"></script>
<script>
$('.slider-for').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: false,
  fade: true,
  asNavFor: '.slider-nav'
});
$('.slider-nav').slick({
  slidesToShow: 3,
  slidesToScroll: 1,
  asNavFor: '.slider-for',
  dots: false,
  centerMode: false,
  focusOnSelect: true
});
//related	
$('.promotion-book').slick({
  dots: false,
  infinite: true,
  arrows: true,
  speed: 300,
  autoplaySpeed: 5000,
  slidesToShow: 5,
  slidesToScroll: 5,
  //variableWidth: true,
  responsive: [
    {
      breakpoint: 800,
      settings: {
	  	//centerPadding: '60px',
        slidesToShow: 3,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 480,
      settings: {
	  	//centerPadding: '40px',
        slidesToShow: 2,
        slidesToScroll: 1
      }
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ]
});

</script>
<!-- /js -->

</body>
</html>
