<!DOCTYPE HTML>
<html dir="ltr" lang="th">
<!-- Top Head -->
<?php include("incs/head-top.html") ?>
<!-- /Top Head -->

<body class="page-category">
<script>
  //<![CDATA[
  $(document).ready(function(){
    $('#navigation>ul>li:nth-child(2)>a').addClass('selected');
  });
  //]]>
</script>
<!-- Headbar -->
<?php include("incs/header.html") ?>
<!-- /Headbar -->

   <div id="toc">

        <nav class="nav-main-cate">
            <ul class="cate-tabs d-flex flex-nowrap center-xs">
                <li><a class="selected" href="#" title="ขายดี">ขายดี</a></li>
                <li><a href="#" title="แนะนำ">แนะนำ</a></li>
                <li><a href="#" title="หนังสือการ์ตูน">หนังสือการ์ตูน</a></li>
                <li><a href="#" title="หนังสือนิยาย">หนังสือนิยาย</a></li>
                <li><a href="#" title="อีบุ๊คการ์ตูน">อีบุ๊คการ์ตูน</a></li>
                <li><a href="#" title="อีบุ๊คนิยาย">อีบุ๊คนิยาย</a></li>
                    
            </ul>
        </nav>
        <div class="container">
        <div class="sub-filter d-flex middle-xs">
            <a class="ui-btn-red" href="javascript:;" data-fancybox="" data-src="#srh-filter" title="บัตรเครดิต / เดบิต"><i class="yicon mr10-xs"><img src="assets/imgs/ic-filter.png" height="16"></i> กรองข้อมูล</a> 
            <nav>
                <a href="#" title="โรแมนติก">โรแมนติก</a>
                <a href="#" title="แฟนตาซี">แฟนตาซี</a>
                <a href="#" title="ผจญภัย">ผจญภัย</a>
                <a href="#" title="คอมมาดื้">คอมมาดื้</a>
            </nav>
        </div>
        <!-- filter popup -->
        <div id="srh-filter" class="filter-popup">
            <div class="inner">
                <form methid="get" action="javascript:;">
                <fieldset>
                    <legend class="t-red"><i class="yicon mr10-xs"><img src="assets/imgs/ic-filter-red.png" height="16"></i> กรองข้อมูล</legend>  
                    <ul class="filter-group">
                        <li>
                            <b>หมวดหมู่</b>
                            <ul class="filter-chk mz-chk">
                                <li><input type="checkbox" value="1" id="flr1" name="flr1"> <label class="ml10-xs" for="flr1">การ์ตูน</label></li>
                                <li><input type="checkbox" value="2" id="flr2" name="flr2"> <label class="ml10-xs" for="flr2">โรแมนติก</label></li>
                                <li><input type="checkbox" value="3" id="flr3" name="flr3"> <label class="ml10-xs" for="flr3">แฟนตาซี</label></li>
                                <li><input type="checkbox" value="4" id="flr4" name="flr4"> <label class="ml10-xs" for="flr4">ผจญภัย</label></li>
                                <li><input type="checkbox" value="5" id="flr5" name="flr5"> <label class="ml10-xs" for="flr5">คอมมาดื้</label></li>
                                <li><input type="checkbox" value="6" id="flr6" name="flr6"> <label class="ml10-xs" for="flr6">สยองขวัญ</label></li>
                                <li><input type="checkbox" value="7" id="flr7" name="flr7"> <label class="ml10-xs" for="flr7">ประวัติศาสตร์</label></li>
                                <li><input type="checkbox" value="8" id="flr8" name="flr8"> <label class="ml10-xs" for="flr8">ไซไฟ</label></li>
                                <li><input type="checkbox" value="9" id="flr9" name="flr9"> <label class="ml10-xs" for="flr9">สงคราม</label></li>

                            </ul>
                        </li>
                        <li>
                            <b>สำนักพิมพ์</b>
                            <ul class="filter-chk mz-chk">
                                <li><input type="checkbox" value="1" id="flr2-1" name="flr2-1"> <label class="ml10-xs" for="flr2-1">siaminter book</label></li>
                                <li><input type="checkbox" value="2" id="flr2-2" name="flr2-2"> <label class="ml10-xs" for="flr2-2">i am book</label></li>
                                <li><input type="checkbox" value="3" id="flr2-3" name="flr2-3"> <label class="ml10-xs" for="flr2-3">siaminter comics light</label></li>
                                <li><input type="checkbox" value="4" id="flr2-4" name="flr2-4"> <label class="ml10-xs" for="flr2-4">co-novel</label></li>
                                <li><input type="checkbox" value="5" id="flr2-5" name="flr2-5"> <label class="ml10-xs" for="flr2-5">บุปผาแห่งรัก</label></li>
                            </ul>
                        </li>
                        <li>
                            <b>ราคา</b>
                            <ul class="filter-chk mz-chk">
                                <li><input type="checkbox" value="1" id="flr3-1" name="flr3-1"> <label class="ml10-xs" for="flr3-1">0 - 99 บาท</label></li>
                                <li><input type="checkbox" value="2" id="flr3-2" name="flr3-2"> <label class="ml10-xs" for="flr3-2">100 - 199 บาท</label></li>
                                <li><input type="checkbox" value="3" id="flr3-3" name="flr3-3"> <label class="ml10-xs" for="flr3-3">200 - 299 บาท</label></li>
                                <li><input type="checkbox" value="4" id="flr3-4" name="flr3-4"> <label class="ml10-xs" for="flr3-4">300 - 399 บาท</label></li>
                                <li><input type="checkbox" value="5" id="flr3-5" name="flr3-5"> <label class="ml10-xs" for="flr3-5">400 - 499 บาท</label></li>
                            </ul>
                        </li>
                        <li class="d-flex center-xs"><button type="reset" class="ui-btn-red">Reset</button></li>
                    </ul>
                </fieldset>
                </form>
            </div>
        </div>
        <!-- /filter popup -->
		<section class="sec-favorite">

                <div class="head-title d-flex flex-wrap center-xs between-xsh">
                    <h2 class="h-topic _self-cl-xs-12 _self-cl-xsh d-flex center-xs start-xsh mb10-xs mb0-sm"><a href="category-list.php" title="หนังสือการ์ตูน แอคชั่น">หนังสือการ์ตูน แอคชั่น</a></h2>

                    <nav class="bar-paging d-flex center-xs end-xsh mb20-xs">
                        <ul class="pagination">
                            <li class="page-item">
                            <a class="page-link" href="#" aria-label="Previous">
                                <span aria-hidden="true"><img src="./assets/imgs/ic-prev-page.png" height="12"></span>
                            </a>
                            </li>
                            <li class="page-item active"><a class="page-link" href="#">1</a></li>
                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item"><a class="page-link" href="#">4</a></li>
                            <li class="page-item"><a class="page-link" href="#">5</a></li>
                            <li class="page-item">
                            <a class="page-link" href="#" aria-label="Next">
                                <span aria-hidden="true"><img src="./assets/imgs/ic-next-page.png" height="12"></span>
                            </a>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div class="contentTabs">
                    <!-- Fav1 -->    
                    <div class="bx-tab" id="shelf1">
                    
                    <div class="cb thm-book row _chd-cl-xs-06-xsh-04-sm-03-md-25 start-xs">
                        <?php for($i=1; $i<=20; $i++) { 

                            $img_url = "assets/contents/thm-book-05.png";
                            $name = "EXE #2 Light Novel";
                            $price = "฿70";
                            $author = "ผู้การ F";
                            switch($i%5)
                            {
                                case "1": 
                                            $img_url = "assets/contents/thm-book-01.png";
                                            $name = "Black Cover บันทึกของกระทิง";
                                            $price = "฿75";
                                            $author = "Jonny Onda";
                                    break;
                                case "2": 
                                            $img_url = "assets/contents/thm-book-02.png";
                                            $name = "Tokyo Ghoul re (เควส)";
                                            $price = "฿70";
                                            $author = "Shin Towada";
                                    break;
                                case "3": 
                                            $img_url = "assets/contents/thm-book-03.png";
                                            $name = "ONE PIECE FILM GOLD";
                                            $price = "฿66.50";
                                            $author = "Eiichiro Oda";
                                    break;
                                case "4": 
                                            $img_url = "assets/contents/thm-book-04.png";
                                            $name = "Executional ตำนานวีรบุรุษโลก(ไม่อยาก)จำ";
                                            $price = "฿149";
                                            $author = "Nightberry";
                                    break;
                            }
                            ?>
                            <article id="ar<?php echo $i; ?>">
                                <div class="in">
                                    <figure><a href="book-detail.php" title="<?php echo $name; ?>"><img src="<?php echo $img_url; ?>" alt="<?php echo $name; ?>"></a>
                                    <i class="tag-new">new</i></figure>
                                    <div class="detail">
                                        <h3><a href="#" title="<?php echo $name; ?>"><?php echo $name; ?></a></h3>
                                        <p class="author"><?php echo $author; ?></p>
                                        <!-- <div class="bar-price">
                                            <span class="price"><b>299</b> บาท</span>
                                            <span class="old">999 บาท</span>
                                        </div> -->
                                    </div>
                                </div>
                            </article>
                            
                        <?php } ?>
                    </div>    

                    <nav class="bar-paging d-flex center-xs end-xsh mt20-xs">
                        <ul class="pagination">
                            <li class="page-item">
                            <a class="page-link" href="#" aria-label="Previous">
                                <span aria-hidden="true"><img src="./assets/imgs/ic-prev-page.png" height="12"></span>
                            </a>
                            </li>
                            <li class="page-item active"><a class="page-link" href="#">1</a></li>
                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item"><a class="page-link" href="#">4</a></li>
                            <li class="page-item"><a class="page-link" href="#">5</a></li>
                            <li class="page-item">
                            <a class="page-link" href="#" aria-label="Next">
                                <span aria-hidden="true"><img src="./assets/imgs/ic-next-page.png" height="12"></span>
                            </a>
                            </li>
                        </ul>
                    </nav>


                    </div>
                    <!-- Fav2 -->
                    <div class="bx-tab" id="shelf2">
                     2
                    </div>
                </div>

		</section>



		</div>
  </div>

<!-- footer -->
<?php include("incs/footer.html") ?>
<?php include("incs/lightbox.html") ?>
<!-- /footer -->
<!-- js -->
<?php include("incs/js.html") ?>
<script src="assets/js/jquery.idTabs.min.js"></script>
<!-- /js -->

</body>
</html>