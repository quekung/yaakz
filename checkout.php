<!DOCTYPE HTML>
<html dir="ltr" lang="th">
<!-- Top Head -->
<?php include("incs/head-top.html") ?>
<!-- /Top Head -->

<body class="page-cart">
<script>
  //<![CDATA[
  $(document).ready(function(){
	  //$('#navigation>ul>li:nth-child(1)>a').addClass('selected');
  });
  //]]>
</script>
<!-- Headbar -->
<?php include("incs/header.html") ?>
<!-- /Headbar -->

   <div id="toc">
		<div class="container">
		<section class="sec-cart">

			<h2 class="h-topic"><span>รายการสินค้า</span></h2>
            <div class="cart-list">
                <ol>
                    <li class="head">
                        <ul>
                            <li class="c1">รายการ</li>
                            <li class="c2">จำนวน</li>
                            <li class="c3">ราคา</li>
                            <li class="c4 hidden-xs"><span class="hid">แก้ไข</span></li>
                        </ul>
                    </li>
                    <?php for($i=1;$i<=3;$i++) { ?> 
                    <li id="or<?php echo $i; ?>">
                        <ul>
                            <li class="c1">
                            <div class="thm-left">
                                <article>
                                    <div class="in">
                                        <figure><a href="#" title="Black Cover บันทึกของกระทิง"><img src="assets/contents/thm-book-0<?php echo $i; ?>.png" alt="Black Cover บันทึกของกระทิง"></a></figure>
                                        <div class="detail">
                                            <h3><a href="#" title="Black Cover บันทึกของกระทิง">Black Cover บันทึกของกระทิง</a></h3>
                                            <div class="type"><i class="yicon"><img src="./assets/imgs/ic-noun-book.png"></i> หนังสือ</div>
                                        </div>
                                    </div>
                                </article>
                            </div>   

                            </li>
                            <li class="c2">1</li>
                            <li class="c3"><div class="t-black"><b class="t-red">259</b> บาท</div></li>
                            <li class="c4 hidden-xs">&nbsp;</li>
                        </ul>
                    </li>    
                    <?php } ?>

                    <li class="total">
                        <ul>
                            <li class="c1">รวมทั้งหมด</li>
                            <li class="c2">3</li>
                            <li class="c3"><div class="t-black"><b class="t-red">777</b> บาท</div></li>
                            <li class="c4 hidden-xs">&nbsp;</li>
                        </ul>
                    </li>
                </ol>
                <div class="ship-detail d-flex _chd-cl-xs-12-xsh-06-md-05 between-xsh mt20-xs">
                    <div class="ship-adr">
                        <h2 class="h-topic"><span>ที่อยู่สำหรับจัดส่ง</span></h2>
                        <big class="d-block address">
                            <b class="d-block mb10-xs">สมศักดิ์   ศิษย์ชัชวาลย์</b>
                            <p class="mb0-xs">เบอร์ติดต่อ : <a href="tel:0818256614" class="f-bold">0818256614</a></p>
                            <p class="mb0-xs">อีเมล : <a href="mailto:somsak@gmail.com" class="f-bold">somsak@gmail.com</a></p>
                            <address>
                            333 ซอยไผ่สิงโต ถนนพระราม4 คลองตัน กรุงเทพ 10110
                            </address>
                        </big>
                    </div>
                    <div class="ship-pay">
                        <h2 class="h-topic"><span> วิธีการชำระเงิน</span></h2>
                        <ul class="form-payment _chd-mb10">
                            <li class="mt05-xs _flex _chd-mr20">
                                <span class="mz-chk"><input type="radio" name="payment" id="g1"> <label class="ml10-xs" for="g1">ชำระด้วยบัตรเครดิต /  เดบิต</label></span>
                                <span class="mz-chk"><input type="radio" name="payment" id="g2"> <label class="ml10-xs" for="g2">ชำระด้วยการโอนเงิน</label></span>
                            </li>
                            <li>
                                <label for="card_no" class="hid">เลขที่บัตร 16 หลัก</label>
                                <input type="tel" class="txt-box" id="card_no" name="card_no" placeholder="เลขที่บัตร 16 หลัก">
                            </li>
                            <li>
                                <label for="card_name" class="hid">ชื่อเจ้าของบัตร</label>
                                <input type="text" class="txt-box" id="card_name" name="card_name" placeholder="ชื่อเจ้าของบัตร">
                            </li>
                            <li class="row _chd-cl-xs-06-xsh-05">
                                <span>
                                    <label for="card_expire" class="hid">เดือน / ปี  ที่หมดอายุ</label>
                                    <input type="text" class="txt-box" id="card_expire" name="card_expire" placeholder="เดือน / ปี  ที่หมดอายุ">
                                </span>
                                <span class="_self-cl-xsh-03">
                                    <label for="card_cvc" class="hid">เดือน / ปี  ที่หมดอายุ</label>
                                    <input type="tel" class="txt-box" id="card_cvc" name="card_cvc" placeholder="CVC">
                                </span>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="ctrl-btn d-flex center-xs mt20-xs _chd-cl-xs-06-sm-04-md-03-lg-02">
                    <span><a href="cart.php" class="ui-btn-dark btn-md btn-block">ย้อนกลับ</a></span>
                    <span><a href="javascript:;" data-fancybox="" data-src="#popup-success" class="ui-btn-blue btn-md btn-block">ดำเนินการชำระเงิน</a></span>
                </div>
            </div> 
                

		</section>



		</div>
  </div>

<!-- footer -->
<?php include("incs/footer.html") ?>
<?php include("incs/lightbox.html") ?>
<!-- confirm delete-->
<div id="popup-delete-cart" class="thm-popup">
<div class="box-middle">
<div class="inner md-read pa20-xs pa30-md txt-c">
	<i class="icon"><img src="assets/imgs/ic-trash.png" height="80"></i>
	<h2 class="head t-black">ลบรายจากออกจากตระกร้าสินค้า</h2>
	<div class="msg txt-c pa30-xs _flex center-xs">
		<p class="_self-cl-xs-12-md-10"><small>ยืนยันการลบรายการนี้ออกจากตระกร้าสินค้า</small></p>
	</div>
	<p class="mt30-xs d-flex center-xs _chd-cl-xs-06-sm-05-md-04">
		<span><a class="ui-btn-red btn-lg btn-block" href="javascript:;" data-fancybox-close="" onClick="parent.jQuery.fancybox.close(); $('#ar1').remove();">ยืนยัน</a></span></p>
</div>
</div>
</div>
<!-- /confirm delete-->
<!-- /footer -->
<!-- js -->
<?php include("incs/js.html") ?>

<!-- /js -->

</body>
</html>