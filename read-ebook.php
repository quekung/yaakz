<!DOCTYPE HTML>
<html dir="ltr" lang="th">
<!-- Top Head -->
<?php include("incs/head-top.html") ?>
<link href="https://fonts.googleapis.com/css?family=Sarabun|Chakra+Petch:300|Taviraj:200" rel="stylesheet">
<!-- /Top Head -->

<body class="page-book">
	<script>
		//<![CDATA[
		$(document).ready(function() {
			$('#navigation>ul>li:nth-child(2)>a').addClass('selected');
		});
		//]]>
	</script>
	<!-- Headbar -->
	<?php include("incs/header.html") ?>
	<!-- /Headbar -->


	<div id="toc" class="pt10">

		<div class="container">
			<div class="crumb"><a href="#">หน้าแรก</a> / <a href="#">นิยาย</a> / <a href="#">นิยายแฟนตาซี</a> / <a href="#">Executional ตำนานวีรบุรุษโลก(ไม่อยาก)จำ</a> / <span>ตอนที่ 1 กินคนได้?</span></div>

			<section class="sec-read-full pt0">
				<div class="box-wh pd0-xs">
					<header class="bar-head-title _flex start-xs">
						<h1>
							<span class="hid">Executional ตำนานวีรบุรุษโลก(ไม่อยาก)จำ</span>
							ตอนที่ 1 : กินคนได้?
						</h1>
					</header>

					<article class="content">
						<div class="reader body-text">

                            <figure>
                                <img src="./assets/contents/user-read-PDF-ep1.jpg" alt="PDF">
                            </figure>
						</div>

					</article>
					<div class="bar-ctrl-page ft _flex between-xs">
						<div class="col-l">
							<a href="#" title="ตอนก่อนหน้า"><i class="yicon"><img src="./assets/imgs/nav-prev-gr.png" height="20"></i><span class="hidden-xs"> ตอนก่อนหน้า</span></a>
						</div>
						<div class="col-r">
							<a href="#" title="ตอนที่ 2: แขกผู้มีเกียรติ ">ตอนต่อไป <i class="yicon"><img src="./assets/imgs/nav-next-gr.png" height="20"></i></a>
						</div>
					</div>
					<div class="tools-read">
					<div class="sw-ep">
						<a id="ep-list" class="dropbtn" href="javascript:;" onclick="callEP()">
							<img src="./assets/contents/thm-book-01.png" alt="invoice">
							<div class="info">
								<i class="yicon"><img src="./assets/imgs/ic-angle-up.png" height="9"></i>
								<h2><small class="d-block">Executional</small> ตำนานวีรบุรุษโลก(ไม่อยาก)จำ</h2>
							</div>
						</a>
						<div id="myEP" class="dropdown-content">

							<ul class="show-list">
								<li><a class="active" href="read-novel.php?1" title="ตอนที่ 1: กินคนได้?">
									<h3>ตอนที่ 1: กินคนได้?</h3>
									<span class="t-blue">อ่านฟรี</span>
								</a></li>
								<li><a href="read-novel.php?1" title="ตอนที่ 2: แขกผู้มีเกียรติ">
									<h3>ตอนที่ 2: แขกผู้มีเกียรติ</h3>
									<span class="t-blue">อ่านฟรี</span>
								</a></li>
								<li><a href="read-novel.php?1" title="ตอนที่ 3: xxx">
									<h3>ตอนที่ 3: xxx</h3>
									<span class="t-red">ซื้อแล้ว</span>
								</a></li>
								<li><a href="javascript:;" data-fancybox="" data-src="#popup-topup-buy" title="ตอนที่ 4: xxx">
									<h3>ตอนที่ 4: อันตรายระหว่างทาง  อันตราย...</h3>
									<span class="t-gray">10 เหรียญ</span>
								</a></li>
								<li><a href="javascript:;" data-fancybox="" data-src="#popup-topup-buy" title="ตอนที่ 4: xxx">
									<h3>ตอนที่ 5: อันตรายระหว่างทาง  อันตราย...</h3>
									<span class="t-gray">10 เหรียญ</span>
								</a></li>

								<li><a href="javascript:;" data-fancybox="" data-src="#popup-topup-buy" title="ตอนที่ 4: xxx">
									<h3>ตอนที่ 6: อันตรายระหว่างทาง  อันตราย...</h3>
									<span class="t-gray">10 เหรียญ</span>
								</a></li>
								<li><a href="javascript:;" data-fancybox="" data-src="#popup-topup-buy" title="ตอนที่ 4: xxx">
									<h3>ตอนที่ 7: อันตรายระหว่างทาง  อันตราย...</h3>
									<span class="t-gray">10 เหรียญ</span>
								</a></li>
								<li><a href="javascript:;" data-fancybox="" data-src="#popup-topup-buy" title="ตอนที่ 4: xxx">
									<h3>ตอนที่ 8: อันตรายระหว่างทาง  อันตราย...</h3>
									<span class="t-gray">10 เหรียญ</span>
								</a></li>
								<li><a href="javascript:;" data-fancybox="" data-src="#popup-topup-buy" title="ตอนที่ 4: xxx">
									<h3>ตอนที่ 9: อันตรายระหว่างทาง  อันตราย...</h3>
									<span class="t-gray">10 เหรียญ</span>
								</a></li>
								<li><a href="javascript:;" data-fancybox="" data-src="#popup-topup-buy" title="ตอนที่ 4: xxx">
									<h3>ตอนที่ 10: อันตรายระหว่างทาง  อันตราย...</h3>
									<span class="t-gray">10 เหรียญ</span>
								</a></li>
								
							</ul>
						</div>
					</div>


					<span class="share"><a id="bigger" class="btn" href="javascript:;" title="ซูมเข้า"><i class="icb-zoomin"><img src="./assets/imgs/ic-zoom-in.png" height="24"></i><small class="d-block">ซูมเข้า</small></a></span>
					<span class="share"><a id="smaller" class="btn" href="javascript:;" title="ซูมออก"><i class="icb-zoomout"><img src="./assets/imgs/ic-zoom-out.png" height="24"></i><small class="d-block">ซูมออก</small></a></span>
                    <span class="share"><a id="fullscreen" class="btn" href="javascript:;" title="ขยายจอ"><i class="icb-full"><img src="./assets/imgs/ic-fullscreen.png" height="24"></i><small class="d-block">ขยายจอ</small></a></span>
					
					

					<span class="share"><a class="btn" href="javascript:;" data-fancybox="share" data-src="#popup-share" title="แชร์เรื่องนี้"><i class="icb-srh"><img src="./assets/imgs/ic-share-bar.png" height="24"></i> <small class="d-block">แชร์</small></a></span>
				</div>




				</div>
			</section>


		</div>
	</div>

	<!-- footer -->
	<?php include("incs/footer.html") ?>
	<?php include("incs/lightbox.html") ?>
	<!-- /footer -->
	<!-- js -->
	<?php include("incs/js.html") ?>
	<script src="assets/js/full-reader.js"></script>
	<!-- /js -->

</body>

</html>