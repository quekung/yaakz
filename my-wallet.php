<!DOCTYPE HTML>
<html dir="ltr" lang="th">
<!-- Top Head -->
<?php include("incs/head-top.html") ?>
<!-- /Top Head -->

<body class="page-profile">
<script>
  //<![CDATA[
  $(document).ready(function(){
	  //$('#navigation>ul>li:nth-child(1)>a').addClass('selected');
  });
  //]]>
</script>
<!-- Headbar -->
<?php include("incs/header.html") ?>
<!-- /Headbar -->

   
   
   <div id="toc">
		<div class="container">
		<section class="sec-wallet pt0">
            <div class="z-my-wallet">
				<h2 class="h-topic"><span>กระเป๋าเงินของท่าน</span></h2>
                <div class="my-wallet row _chd-cl-xs-08 middle-xs">

					<figure class="_self-cl-xs-04 _flex center-xs">
						<div class="_self-cl-xs-12-sm-11-pd0 _flex center-xs top-xs"><img src="assets/imgs/wallet-info.png" alt="กระเป๋าเงินของฉัน"></div>
					</figure>
					<div class="my-detail txt-l">
						<div class="coin-credit">
							<big><i class="yicon mr10-xs mr20-md"><img src="assets/imgs/ic-large-coin.png" height="60"></i> 
                            จำนวนเหรียญคงเหลือ <b class="t-black pl10-xs pr10-xs"><em>1000</em> เหรียญ</b>
                            </big>
						</div>
						
					</div>

				</div>
            </div>
            <div class="z-topup">
				<h2 class="h-topic"><span>ช่องทางการเติมเหรียญ</span></h2>
                <ul class="list-payment d-flex center-xs around-md _chd-cl-xs-12-xsh-04-md-03">
                    <li class="pay1">
                        <a href="javascript:;" data-fancybox="" data-src="#popup-topup-select" title="บัตรเครดิต / เดบิต">
                            <figure>
                                <figcaption>บัตรเครดิต / เดบิต</figcaption>
                                <img src="assets/imgs/pay-credit.png" alt="บัตรเครดิต / เดบิต" height="160">
                            </figure>
                            <span class="ui-btn-blue btn-block btn-md">เติมเหรียญ</span>
                        </a>
                    </li>
                    <li class="pay2">
                        <a href="javascript:;" data-fancybox="" data-src="#popup-topup-select" title="QR Code">
                            <figure>
                                <figcaption>QR Code</figcaption>
                                <img src="assets/imgs/pay-qrcode.png" alt="QR Code" height="160">
                            </figure>
                            <span class="ui-btn-blue btn-block btn-md">เติมเหรียญ</span>
                        </a>
                    </li>
                    <li class="pay3">
                        <a href="javascript:;" data-fancybox="" data-src="#popup-topup-select" title="โอนเงิน">
                            <figure>
                                <figcaption>โอนเงิน</figcaption>
                                <img src="assets/imgs/pay-transfer.png" alt="โอนเงิน" height="160">
                            </figure>
                            <span class="ui-btn-blue btn-block btn-md">เติมเหรียญ</span>
                        </a>
                    </li>

                </ul>
            </div>
            <div class="z-w-history">
				<h2 class="h-topic"><span>ประวัติการเติมเงิน</span></h2>
                <div class="box-wh pd0 mb30-lg mb10-xs">
				<div class="table-drag">
				    <table class="table table-history tb-hover" width="100%" border="0">
                        <thead>
                        <tr>
                        <th scope="col" valign="middle" align="left">วันที่และเวลา</th>
                        <th scope="col" valign="middle" align="left">ช่องทางการเติมเงิน</th>
                        <th scope="col" valign="middle" align="left">แพ็คเกจเหรียญ</th>

                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                        <td valign="middle">10 มิ.ย. 2021 14:41 น.</td>
                        <td valign="middle">โอนเงิน ธนาคารกสิกรไทย</td>
                        <td valign="middle"><a href="#" class="t-black">เติม 300 บาท  ได้รับเหรียญทั้งหมด 330 เหรียญ (10%)</a></td>
                        </tr>
                        <tr>
                        <td valign="middle">10 มิ.ย. 2021 14:41 น.</td>
                        <td valign="middle">โอนเงิน ธนาคารกสิกรไทย</td>
                        <td valign="middle"><a href="#" class="t-black">เติม 300 บาท  ได้รับเหรียญทั้งหมด 330 เหรียญ (10%)</a></td>
                        </tr>
                        <tr>
                        <td valign="middle">10 มิ.ย. 2021 14:41 น.</td>
                        <td valign="middle">โอนเงิน ธนาคารกสิกรไทย</td>
                        <td valign="middle"><a href="#" class="t-black">เติม 300 บาท  ได้รับเหรียญทั้งหมด 330 เหรียญ (10%)</a></td>
                        </tr>
                        <tr>
                        <td valign="middle">10 มิ.ย. 2021 14:41 น.</td>
                        <td valign="middle">โอนเงิน ธนาคารกสิกรไทย</td>
                        <td valign="middle"><a href="#" class="t-black">เติม 300 บาท  ได้รับเหรียญทั้งหมด 330 เหรียญ (10%)</a></td>
                        </tr>
                        <tr>
                        <td valign="middle">10 มิ.ย. 2021 14:41 น.</td>
                        <td valign="middle">โอนเงิน ธนาคารกสิกรไทย</td>
                        <td valign="middle"><a href="#" class="t-black">เติม 300 บาท  ได้รับเหรียญทั้งหมด 330 เหรียญ (10%)</a></td>
                        </tr>
                        <tr>
                        <td valign="middle">10 มิ.ย. 2021 14:41 น.</td>
                        <td valign="middle">โอนเงิน ธนาคารกสิกรไทย</td>
                        <td valign="middle"><a href="#" class="t-black">เติม 300 บาท  ได้รับเหรียญทั้งหมด 330 เหรียญ (10%)</a></td>
                        </tr>
                        
                        </tbody>
                    </table>

            </div>
    </div>
            </div>
                
		</section>



		</div>
  </div>

<!-- footer -->
<?php include("incs/footer.html") ?>
<?php include("incs/lightbox.html") ?>
<!-- /footer -->
<!-- js -->
<?php include("incs/js.html") ?>
<!-- /js -->

</body>
</html>