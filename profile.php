<!DOCTYPE HTML>
<html dir="ltr" lang="th">
<!-- Top Head -->
<?php include("incs/head-top.html") ?>
<!-- /Top Head -->

<body class="page-profile">
<script>
  //<![CDATA[
  $(document).ready(function(){
	  //$('#navigation>ul>li:nth-child(1)>a').addClass('selected');
  });
  //]]>
</script>
<!-- Headbar -->
<?php include("incs/header.html") ?>
<!-- /Headbar -->

   
   
   <div id="toc">
		<div class="container">
		<section class="sec-01 pt0">

				<h2 class="h-topic"><span>จัดการบัญชี</span></h2>
                <ul class="my-tabs mb20-xs mb30-md">
                    <li><a class="selected" href="profile.php" title="ข้อมูลส่วนตัว">ข้อมูลส่วนตัว</a></li>
                    <li><a href="my-order-history.php" title="รายการคำสั่งซื้อล่าสุด">รายการคำสั่งซื้อล่าสุด</a></li>
                </ul>
                <form class="wrap-profile" method="post" action="">
                    <div class="edit-form">
                            <fieldset>
                                <legend class="hid">แก้ไขข้อมูล</legend>
                                <div class="avatar" data-text="เลือกรูปโปรไฟล์">
                                    <img id="show_image_src" src="./assets/imgs/avatar_default.png" alt="avatar">
                                    <div class="edit-pic">
                                        <label for="avatar_upload"><i class="yicon"><img src="./assets/imgs/ic-camera.png" height="20"></i> แก้ไข</label>
                                        <input type="file" class="file-upload-field" id="avatar_upload" name="avatar_upload">
                                    </div>
                                </div>
                                <script>
                                    document.getElementById("avatar_upload").onchange = function () {
                                    var reader = new FileReader();
                                    reader.onload = function (e) {
                                        document.getElementById("show_image_src").src = e.target.result;
                                    };
                                    reader.readAsDataURL(this.files[0]);
                                };    
                                /*input file*/
                                $("form").on("change", ".file-upload-field", function(){ 
                                    $(this).parent(".avatar").attr("data-text", $(this).val().replace(/.*(\/|\\)/, '') ).addClass("added");
                                    $('.avatar .image').removeClass('hid');
                                });

                                </script>
                                <ul class="_chd-mb30">
                                    <!-- <li>
                                    <label for="u_display">ชื่อแสดงในระบบ</label>
                                    <input type="text" maxlength="100" name="u_display" id="u_display" class="txt-box block" value="saruns">
                                    </li> -->
                                    <li class="d-flex between-xs">
                                    <span>
                                        <label for="u_name">ชื่อ <span class="rq">*</span></label>
                                        <input type="text" maxlength="100" name="u_name" id="u_name" class="txt-box block" value="สมศักดิ์">
                                    </span>
                                    <span>
                                        <label for="u_sname">นามสกุล <span class="rq">*</span></label>
                                        <input type="text" maxlength="100" name="u_sname" id="u_sname" class="txt-box block" value="ศิษย์ชัชวาลย์">
                                    </span>
                                    </li>
                                    <li class="d-flex between-xs">
                                    <span>
                                        <label for="u_gender">เพศ <span class="rq">*</span></label>
                                        <div class="mt05-xs _flex _chd-mr20">
                                            <span class="mz-chk"><input type="radio" name="gender" id="g1"> <label class="ml10-xs" for="g1">ชาย</label></span>
                                            <span class="mz-chk"><input type="radio" name="gender" id="g2"> <label class="ml10-xs" for="g2">หญิง</label></span>
                                            <span class="mz-chk"><input type="radio" name="gender" id="g3"> <label class="ml10-xs" for="g3">ไม่ระบุ</label></span>
                                        </div>
                                    </span>
                                    <span>
                                        <label for="u_email">อีเมล แอดเดรส</label>
                                        <input type="text" maxlength="100" name="u_email" id="u_email" class="txt-box block bg-gray" value="sarunhaha@gmail.com" readonly="">
                                    </span>
                                    </li>
                                    <li class="d-flex between-xs">
                                    <span>
                                        <label for="u_tel">เบอร์โทรศัพท์ติดต่อ</label>
                                        <input type="text" maxlength="10" name="u_tel" id="u_tel" class="txt-box block" placeholder="ระบุเบอร์โทร">
                                    </span>
                                    <span>
                                        <label for="u_email">รหัสผ่าน</label>
                                        <div class="d-flex"><b class="mr30-xs">************</b> <a class="t-orange" href="javascript:;" data-fancybox="editPW" data-src="#popup-changepassword" title="ต้องการเปลี่ยนรหัสผ่าน?"><u>ต้องการเปลี่ยนรหัสผ่าน?</u></a></div>
                                    </span>
                                    </li>

                                    
                                    <li class="interest _self-mb0">
                                        <label for="regis_interest">เลือกหมวดหมู่ที่ท่านสนใจ</label>
                                        <ol class="chl-interest row mz-chk _chd-cl-xs-06-sm-03-md-25-mt10">
                                            <li><input type="checkbox" value="1" id="itr1" name="itr1"> <label class="ml10-xs" for="itr1">แฟนตาซี</label></li>
                                            <li><input type="checkbox" value="2" id="itr2" name="itr2"> <label class="ml10-xs" for="itr2">สยองขวัญ</label></li>
                                            <li><input type="checkbox" value="3" id="itr3" name="itr3"> <label class="ml10-xs" for="itr3">กำลังภายใน</label></li>
                                            <li><input type="checkbox" value="4" id="itr4" name="itr4"> <label class="ml10-xs" for="itr4">ประวัติศาสตร์</label></li>
                                            <li><input type="checkbox" value="5" id="itr5" name="itr5"> <label class="ml10-xs" for="itr5">รัก โรแมนติค</label></li>
                                            <li><input type="checkbox" value="6" id="itr6" name="itr6"> <label class="ml10-xs" for="itr6">สืบสวนสอบสวน</label></li>
                                            <li><input type="checkbox" value="7" id="itr7" name="itr7"> <label class="ml10-xs" for="itr7">คอมมาดื้</label></li>
                                            <li><input type="checkbox" value="8" id="itr8" name="itr8"> <label class="ml10-xs" for="itr8">ผจญภัย</label></li>
                                            <li><input type="checkbox" value="9" id="itr9" name="itr9"> <label class="ml10-xs" for="itr9">พัฒนาตนเอง</label></li>
                                            <li><input type="checkbox" value="10" id="itr10" name="itr10"> <label class="ml10-xs" for="itr10">ดราม่า</label></li>
                                        </ol>
                                    </li>
                                    
                                    <!-- <li>
                                    <label for="bio" class="block mb10-xs">เกี่ยวกับคุณ</label>
                                    <textarea name="bio" id="bio" placeholder="บอกเรื่องราวเกี่ยวกับตัวคุณสั้นๆ ข้อมูลส่วนนี้จะแสดงสาธารณะ" rows="10"></textarea>
                                    </li> -->
                                    <li class="space-top">
                                        <h3 class="head mb30-xs">ที่อยู่จัดส่งสินค้า</h3>
                                        <label for="u_address">ที่อยู่</label>
                                        <input type="text" maxlength="200" name="u_address" id="u_address" class="txt-box block" placeholder="ระบุ บ้านเลขที่ , ซอย , ถนน">
                                    </li>
                                    <li class="d-flex between-xs">
                                    <span>
                                        <label for="u_province">จังหวัด</label>
                                        <input type="text" name="u_province" id="u_province" class="txt-box block" placeholder="เลือกจังหวัด">
                                    </span>
                                    <span>
                                        <label for="u_district">เขต / อำเภอ</label>
                                        <input type="text" name="u_district" id="u_district" class="txt-box block" placeholder="เขต / อำเภอ">
                                    </span>
                                    </li>
                                    <li class="d-flex between-xs">
                                    <span>
                                        <label for="u_sub-district">แขวง / ตำบล</label>
                                        <input type="text" name="u_sub-district " id="u_sub-district" class="txt-box block" placeholder="แขวง / ตำบล">
                                    </span>
                                    <span>
                                        <label for="u_postcode">รหัสไปรษณีย์</label>
                                        <input type="text" name="u_postcode" id="u_postcode" class="txt-box block" placeholder="ระบุรหัสไปรษณีย์">
                                    </span>
                                    </li>
                                    
                                    <li class="ctrl-btn d-flex _chd-cl-xs-07-sm-05-md-04-lg-03 center-xs mt20-xs"> 
                                        <span><input type="button" class="ui-btn-blue btn-lg btn-block" id="button-signup-pop" value="อัพเดตข้อมูล"></span>
                                    </li>
                                </ul>
                            </fieldset>
                        </div>
                </form>

		</section>



		</div>
  </div>

<!-- footer -->
<?php include("incs/footer.html") ?>
<?php include("incs/lightbox.html") ?>
<!-- /footer -->
<!-- js -->
<?php include("incs/js.html") ?>
<!-- /js -->

</body>
</html>